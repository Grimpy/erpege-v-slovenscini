import json
import glob
import csv
from Cheetah.Template import Template


TRANSLATIONS = {}
MONSTER_LIST = []
ABILITIES = {
    "DEX": "Gib",
    "CON": "Kon",
    "WIS": "Mod",
    "CHA": "Kar",
    "STR": "Moč",
    "INT": "Int",
}


def get_translation(key: str):
    key = key.strip()
    return TRANSLATIONS.get(key.lower(), key)


def to_meters(f: float):
    m = f * 0.3048
    m = round(m)
    return m


def populate_payload(monster, payload) -> bool:
    name = monster["name"]
    if name.lower() not in MONSTER_LIST:
        return False
    payload["monster_name"] = get_translation(name)

    size = get_translation(monster["size"])
    tp = get_translation(monster["type"])
    payload["monster_type"] = f"{size} {tp}"

    alignment = ""
    alignment_parts = monster["alignment"].split(" ")
    for i in alignment_parts:
        if i == "evil":
            i = "zloben"
        elif i == "good":
            i = "dober"
        elif i == "any":
            i = "karkoli"
        elif i == "unaligned":
            i = "neopredeljen"
        elif i == "chaotic":
            i = "kaotičen"
        # i = get_translation(i)
        alignment += " " + i
    payload["monster_alignment"] = alignment

    armor_class = ""
    for i, a in enumerate(monster["armor_class"]):
        if i > 0:
            armor_class += ", "
        tp = get_translation(a["type"])
        if tp == "natural":
            tp = "naravni oklep"
        v = a["value"]
        armor_class += f"{v} ({tp})"
    payload["armor_class"] = armor_class

    hit_points = str(monster["hit_points"])
    if monster["hit_points_roll"]:
        cleaned = monster["hit_points_roll"].replace("d", "k")
        hit_points += f" ({cleaned})"
    payload["hit_points"] = hit_points

    speed = ""
    for i, key in enumerate(monster["speed"].keys()):
        tp = get_translation(key)
        v = monster["speed"][key]
        if not isinstance(v, bool):
            v = v.replace(" ft.", "")
            v = to_meters(int(v))
        if i > 0 and speed != "":
            speed += ", "
        if isinstance(v, bool):
            speed += f"{tp}"
        else:
            speed += f"{v} m {tp}"
    payload["speed"] = speed

    payload["strength"] = monster["strength"]
    payload["dexterity"] = monster["dexterity"]
    payload["constitution"] = monster["constitution"]
    payload["intelligence"] = monster["intelligence"]
    payload["wisdom"] = monster["wisdom"]
    payload["charisma"] = monster["charisma"]

    saving_throws = ""
    skills = ""
    for proficiency in monster["proficiencies"]:
        val = proficiency["value"]
        prof = proficiency["proficiency"]
        s = prof["name"].split(": ")[1]
        s = get_translation(s)
        if "Skill" in prof["name"]:
            if skills != "":
                skills += ", "
            skills += f"{s} {val}"
        if "Saving Throw" in prof["name"]:
            if saving_throws != "":
                saving_throws += ", "
            saving_throws += f"{ABILITIES[s]} {val}"
    payload["saving_throws"] = saving_throws
    payload["skills"] = skills

    damage_vulnerabilities = ""
    for d in monster["damage_vulnerabilities"]:
        d = get_translation(d)
        if damage_vulnerabilities != "":
            damage_vulnerabilities += ", "
        damage_vulnerabilities += d
    payload["damage_vulnerabilities"] = damage_vulnerabilities

    damage_resistances = ""
    for d in monster["damage_resistances"]:
        d = get_translation(d)
        if damage_resistances != "":
            damage_resistances += ", "
        damage_resistances += d
    payload["damage_resistances"] = damage_resistances

    damage_immunities = ""
    for d in monster["damage_immunities"]:
        d = get_translation(d)
        if damage_immunities != "":
            damage_immunities += ", "
        damage_immunities += d
    payload["damage_immunities"] = damage_immunities

    condition_immunities = ""
    for d in monster["condition_immunities"]:
        d = get_translation(d["name"])
        if condition_immunities != "":
            condition_immunities += ", "
        condition_immunities += d
    payload["condition_immunities"] = condition_immunities

    senses = ""
    for key in monster["senses"].keys():
        v = monster["senses"][key]
        if not isinstance(v, int) and len(v) < 8 and "ft." in v:
            v = v.replace(" ft.", "")
            v = str(to_meters(int(v))) + " m"
        sense = get_translation(key)
        if sense == "passive_perception":
            sense = "pasivno Zaznavanje"
            v = monster["senses"][key]
        if senses != "":
            senses += ", "
        senses += f"{sense} {v}"
    payload["senses"] = senses

    languages = ""
    for l in monster["languages"].split(","):
        l = get_translation(l)
        if languages != "":
            languages += ", "
        languages += l
    payload["languages"] = languages

    payload["challenge"] = monster["challenge_rating"]
    payload["proficiency_bonus"] = monster["proficiency_bonus"]

    abilities = []
    for ability in monster["special_abilities"]:
        name = get_translation(ability["name"])
        desc = ability["desc"]
        abilities.append({"name": name, "desc": desc})
    payload["special_abilities"] = abilities

    actions = []
    for action in monster["actions"]:
        name = get_translation(action["name"])
        a = {
            "name": name,
            "desc": action["desc"],
        }
        if "melee or ranged" in action["desc"].lower():
            a["distance"] = "both"
        elif "melee" in action["desc"].lower():
            a["distance"] = "melee"
        elif "ranged" in action["desc"].lower():
            a["distance"] = "ranged"

        if "weapon" in action["desc"].lower():
            a["type"] = "weapon"
        elif "spell" in action["desc"].lower():
            a["type"] = "spell"

        if "attack_bonus" in action:
            a["mod"] = action["attack_bonus"]
        if "damage" in action:
            try:
                tp = action["damage"][0]["damage_type"]["name"]
                a["dmg_type"] = get_translation(tp)
                a["dmg"] = action["damage"][0]["damage_dice"].replace("d", "k")
            except Exception as e:
                print(e)
                a["dmg_type"] = "TODO"
            if len(action["damage"]) > 1:
                try:
                    tp = action["damage"][1]["damage_type"]["name"]
                    a["plus_dmg"] = action["damage"][1]["damage_dice"].replace("d", "k")
                    a["plus_dmg_type"] = get_translation(tp)
                except Exception as e:
                    print(e)
                    a["plus_dmg"] = "TODO!!!"
        actions.append(a)
    payload["actions"] = actions

    legendaries = []
    for action in monster["legendary_actions"]:
        legendaries.append(
            {"name": get_translation(action["name"]), "desc": action["desc"]}
        )
    payload["legendary_actions"] = legendaries
    return True


with open("/home/gorzi/Prejemi/cleanup.csv") as f:
    csv_reader = csv.reader(f, delimiter="\t")
    for row in csv_reader:
        TRANSLATIONS[row[0].lower()] = row[1]

with open("monsters.txt", "r") as f:
    MONSTER_LIST = list(map(lambda e: e.strip(), f.readlines()))
    print(MONSTER_LIST)

with open("monsters.json", "r") as input_file:
    monsters = json.load(input_file)

with open("monster_block.tex", "r") as template_file:
    template_definition = template_file.read()

content = ""
for monster in monsters:
    payload = {}
    proceed = populate_payload(monster, payload)

    if proceed:
        t = Template(template_definition, searchList=payload)
        content += "\n\n" + str(t)

with open("out.tex", "w") as out:
    out.write(content)
