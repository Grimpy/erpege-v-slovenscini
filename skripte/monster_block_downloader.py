import urllib.request, json

url = "https://www.dnd5eapi.co/api/monsters/"

monsterObjs = []

urlPointer = urllib.request.urlopen(url)
parsed = json.load(urlPointer)


for monster in parsed["results"]:
    monster = monster["index"]
    try:
        urlPointer = urllib.request.urlopen(f"{url}/{monster}")
        parsed = json.load(urlPointer)
        monsterObjs.append(parsed)
    except Exception as e:
        print(f"Could not get {monster}: {e}")

with open("monsters.json", "w") as file:
    file.write(json.dumps(monsterObjs))
