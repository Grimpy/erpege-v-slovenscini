#######################################################
#
# Converts the given CSV file into custom tex glossary format
# and prints the result to standard output
#
#######################################################

import csv
import sys
import locale


locale.setlocale(locale.LC_ALL, 'sl_SI.utf8')


class Entry:
    def __init__(self, english_term: str, slovene_term: str, description: str):
        self.english_term = english_term
        self.slovene_term = slovene_term
        self.description = description


def parse_arguments() -> str:
    arguments = sys.argv
    if len(arguments) == 1:
        print("No input file provided. Exiting...")
        exit(0)
    return arguments[1]


def sort_by_english(element: Entry) -> str:
    return locale.strxfrm(element.english_term.lower())


def sort_by_slovene(element: Entry) -> str:
    return locale.strxfrm(element.slovene_term.lower())


def get_entries_from_file(input_filepath: str) -> list[Entry]:
    entries = []
    with open(input_filepath, 'r', newline='') as input_file:
        data = csv.reader(input_file)
        for row in data:
            if len(row) > 1:
                english_term = row[0]
                slovene_term = row[1]
                description = ''
                if len(row) > 2:
                    description = row[2]
                entry = Entry(english_term, slovene_term, description)
                entries.append(entry)
    return entries


def check_duplicates(entries: list[Entry]):
    entries.sort(key=sort_by_english)

    print("Najdeni duplikati:")
    print("============== Angleški ===================")
    n = len(entries)
    for i in range(n-1):
        current_entry = entries[i]
        next_entry = entries[i+1]

        if current_entry.english_term == next_entry.english_term:
            print(f"{current_entry.english_term} ({i}) - {next_entry.english_term} ({i+1})")

    print("============== Slovenski ===================")
    entries.sort(key=sort_by_slovene)
    for i in range(n - 1):
        current_entry = entries[i]
        next_entry = entries[i + 1]

        if current_entry.slovene_term == next_entry.slovene_term:
            print(f"{current_entry.slovene_term} ({i}) - {next_entry.slovene_term} ({i + 1})")


def main():
    input_filepath = parse_arguments()
    entries = get_entries_from_file(input_filepath)
    check_duplicates(entries)


main()
