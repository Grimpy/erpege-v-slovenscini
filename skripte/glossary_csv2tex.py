#######################################################
#
# Converts the given CSV file into custom tex glossary format
# and prints the result to standard output
#
#######################################################

import csv
import sys
import locale


locale.setlocale(locale.LC_ALL, 'sl_SI.utf8')


class Entry:
    def __init__(self, english_term: str, slovene_term: str, description: str):
        self.english_term = english_term
        self.slovene_term = slovene_term
        self.description = description


def parse_arguments() -> str:
    arguments = sys.argv
    if len(arguments) == 1:
        print("No input file provided. Exiting...")
        exit(0)
    return arguments[1]


def sort_by_english(element: Entry) -> str:
    return locale.strxfrm(element.english_term.lower())


def sort_by_slovene(element: Entry) -> str:
    return locale.strxfrm(element.slovene_term.lower())


def get_entries_from_file(input_filepath: str) -> dict[str, list[Entry]]:
    entries: dict[str, list[Entry]] = {}
    with open(input_filepath, 'r', newline='') as input_file:
        data = csv.reader(input_file)
        for row in data:
            if len(row) > 1:
                english_term = row[0]
                slovene_term = row[1]
                description = ''
                category = 'Splošno'
                if len(row) > 2:
                    description = row[2]
                if len(row) > 3 and row[3] != '':
                    category = row[3]
                entry = Entry(english_term, slovene_term, description)
                if category not in entries:
                    entries[category] = []
                entries[category].append(entry)
    return entries


def convert_to_tex(entries: dict[str, list[Entry]]):
    output = ''

    output += '\\section{Angleško-slovenski}\n\n'
    first = True
    for category, entry_list in entries.items():
        entry_list.sort(key=sort_by_english)

        if not first:
            output += '\n\\newpage\n'
        first = False

        output += '\\subsection{' + category + '}\n'

        letter = ''
        for entry in entry_list:
            if len(entry.english_term) > 0:
                first_letter = entry.english_term[0].upper()
                if first_letter != letter:
                    letter = first_letter
                    output += '\n\\subsubsection{' + letter + '}\n\n\\hspace{10pt}'
            output += '\\gentry{' + entry.english_term + '}{' + entry.slovene_term + '}{' + entry.description + '}\n'

    output += '\n\n\\newpage\n\\section{Slovensko-angleški}\n\n'
    first = True
    for category, entry_list in entries.items():
        entry_list.sort(key=sort_by_slovene)

        if not first:
            output += '\n\\newpage\n'
        first = False

        output += '\\subsection{' + category + '}\n\n'

        letter = ''
        for entry in entry_list:
            if len(entry.slovene_term) > 0:
                first_letter = entry.slovene_term[0].upper()
                if first_letter != letter:
                    letter = first_letter
                    output += '\n\\subsubsection{' + letter + '}\n\n\\hspace{10pt}'
            output += '\\gentry{' + entry.slovene_term + '}{' + entry.english_term + '}{' + entry.description + '}\n'

    print(output)


def main():
    input_filepath = parse_arguments()
    entries = get_entries_from_file(input_filepath)
    convert_to_tex(entries)


main()
