import json

with open("monsters.json", "r") as inputFile:
    monsters = json.load(inputFile)

texContent = ""

abilities = {
    "DEX": "Gib",
    "STR": "Moč",
    "CON": "Kon",
    "INT": "Int",
    "WIS": "Mod",
    "CHA": "Kar",
}

sizes = {
    "Huge": ["Ogromno", "Ogromen", "Ogromna"],
    "Large": ["Veliko", "Velik", "Velika"],
    "Gargantuan": ["Gromozansko", "Gromozanski", "Gromozanska"],
    "Medium": ["Povprečno", "Povprečen", "Povprečna"],
    "Small": ["Majhno", "Majhen", "Majhna"],
    "Tiny": ["Drobno", "Droben", "Drobna"],
}

types = {
    "dragon": "zmaj",
    "celestial": "nebesnik",
    "construct": "konstrukt",
    "elemental": "prvinec",
    "fiend": "mračni stvor",
    "demon": "demon",
    "devil": "zlodej",
    "giant": "velikan",
    "humanoid": "človečnjak",
    "undead": "nemrtvec",
    "aberration": "nakazen",
    "beast": "zver",
    "fey": "vilida",
    "monstrosity": "grozošast",
    "ooze": "žlobudra",
    "plant": "rastlina",
}

alignments = {
    "chaotic": ["kaotično", "kaotičen", "kaotična"],
    "neutral": ["nevtralno", "nevtralen", "nevtralna"],
    "good": ["dobro", "dober", "dobra"],
    "evil": ["zlobno", "zloben", "zlobna"],
    "lawful": ["redoljubno", "redoljuben", "redoljubna"],
    "unaligned": ["neopredeljeno", "neopredeljen", "neopredeljena"],
    "any": ["katerakoli"],
    "alignment": ["opredeljenost"],
}

skillsTranslation = {
    "Athletics": "Atletika",
    "Acrobatics": "Akrobatika",
    "Arcana": "Čudoslovje",
    "Animal Handling": "Čut za živali",
    "Persuasion": "Napeljevanje",
    "Nature": "Naravoslovje",
    "Investigation": "Preiskovanje",
    "Survival": "Preživetje",
    "Stealth": "Prikritost",
    "Sleight of Hand": "Rokohitrstvo",
    "Performance": "Uprizarjanje",
    "Intimidation": "Ustrahovanje",
    "Insight": "Uvid",
    "Religion": "Veroslovje",
    "Deception": "Zavajanje",
    "Perception": "Zaznavanje",
    "Medicine": "Zdravilstvo",
    "History": "Zgodovina",
}


def getSize(a: str, b: str) -> str:
    version = 0
    if b in [
        "dragon",
        "celestial",
        "construct",
        "elemental",
        "fiend",
        "demon",
        "devil",
        "giant",
        "humanoid",
        "undead",
    ]:
        version = 1
    elif b in ["aberration", "beast", "fey", "monstrosity", "ooze", "plant"]:
        version = 2
    return sizes[a][version]


def getType(a: str) -> str:
    return types[a]


def getAlignment(a: str) -> str:
    b = a.split(" ")
    c = ""
    for h in b:
        c += " " + alignments[h][0]
    return c


def getArmor(a: list) -> str:
    out = ""
    for i, b in enumerate(a):
        if i > 0:
            out += ", "
        val = b["value"]
        out += str(val)
        tp = b["type"]
        if tp == "armor":
            out += " ("
            if b.get("desc", None):
                out += b["desc"]
            else:
                for j, l in enumerate(b.get("armor", [])):
                    if j > 0:
                        out += ", "
                    out += f"{l['name']}"
            out += ")"
        elif tp == "natural":
            out += " (naravni oklep)"
    return out


def getSpeed(
    walkSpeed: str, flySpeed: str, climbSpeed: str, swimSpeed: str, canHover: bool
):
    out = ""
    if walkSpeed:
        m = walkSpeed.split(" ")
        meters = int(int(m[0]) * 0.3048)
        out += f"{meters} m"
    else:
        out += f"0 m, "
    if flySpeed:
        m = flySpeed.split(" ")
        meters = int(int(m[0]) * 0.3048)
        if walkSpeed:
            out += ", "
        out += f"letenje {meters} m"
    if canHover:
        out += " (lebdenje)"
    if climbSpeed:
        m = climbSpeed.split(" ")
        meters = int(int(m[0]) * 0.3048)
        if flySpeed or canHover or walkSpeed:
            out += ", "
        out += f"plezanje {meters} m"
    if swimSpeed:
        m = swimSpeed.split(" ")
        meters = int(int(m[0]) * 0.3048)
        if flySpeed or walkSpeed or climbSpeed:
            out += ", "
        out += f"plavanje {meters} m"
    return out


def getProficiencies(profs: list) -> tuple[list, list]:
    savingThrowsOut = []
    skillsOut = []
    for prof in profs:
        if "Saving Throw" in prof["proficiency"]["name"]:
            l = prof["proficiency"]["name"].split(": ")
            s = abilities[l[1]]
            savingThrowsOut.append(f"{s} +{prof['value']}")
        if "Skill" in prof["proficiency"]["name"]:
            l = prof["proficiency"]["name"].split(": ")
            s = skillsTranslation[l[1]]
            skillsOut.append(f"{s} +{prof['value']}")
    return savingThrowsOut, skillsOut


for monster in monsters:
    name = monster["name"]
    size = monster["size"]
    tp = monster["type"]
    alignment = monster["alignment"]
    armorClass = monster["armor_class"]
    hitPoints = monster["hit_points"]
    hitPointsRoll = monster["hit_points_roll"]

    walkSpeed = monster["speed"].get("walk", None)
    flySpeed = monster["speed"].get("fly", None)
    swimSpeed = monster["speed"].get("swim", None)
    climbSpeed = monster["speed"].get("climb", None)
    canHover = monster["speed"].get("hover", False)

    strength = monster["strength"]
    dexterity = monster["dexterity"]
    constitution = monster["constitution"]
    intelligence = monster["intelligence"]
    wisdom = monster["wisdom"]
    charisma = monster["charisma"]

    proficiencies = monster["proficiencies"]

    damageVulnerabilities = monster["damage_vulnerabilities"]
    damageResistances = monster["damage_resistances"]
    damageImmunities = monster["damage_immunities"]
    conditionImmunities = monster["condition_immunities"]
    senses = monster["senses"]
    languages = monster["languages"]
    challengeRating = monster["challenge_rating"]
    xp = monster["xp"]

    specialAbilities = monster["special_abilities"]
    actions = monster["actions"]
    legendaryActions = monster["legendary_actions"]

    skills, savingThrows = getProficiencies(proficiencies)

    tex = f"""\\begin{{DndMonster}}[float*=b,width=\\textwidth + 8pt]{{{name}}}
  \\begin{{multicols}}{{2}}
  """

    monsterType = f"{getSize(size, tp)} {getType(tp)}"

    tex += f"""\\DndMonsterType{{{monsterType}, {getAlignment(alignment)}}}\n"""
    tex += f"""\\DndMonsterBasics[
        armor-class = {{{getArmor(armorClass)}}},
        hit-points  = {{{hitPoints} (\\DndDice{{{hitPointsRoll}}})}},
        speed       = {{{getSpeed(walkSpeed, flySpeed, climbSpeed, swimSpeed, canHover)}}},
      ]
      """
    tex += f"""\\DndMonsterAbilityScores[
        str = {strength},
        dex = {dexterity},
        con = {constitution},
        int = {intelligence},
        wis = {wisdom},
        cha = {charisma},
      ]
      """
    tex += f"""\\DndMonsterDetails[
    """
    if len(savingThrows) > 0:
        tex += f"saving-throws = {{{', '.join(savingThrows)}}},\n"
    if len(skills) > 0:
        tex += f"skills = {{{', '.join(skills)}}},\n"
    if len(damageVulnerabilities) > 0:
        tex += f"damage-vulnerabilities = {{{', '.join(damageImmunities)}}}\n"
    if len(damageResistances) > 0:
        tex += f"damage-resistances = {{{', '.join(damageResistances)}}},\n"
    if len(damageImmunities) > 0:
        tex += f"damage-immunities = {{{', '.join(damageImmunities)}}},\n"
    if len(conditionImmunities) > 0:
        conditionImmunities = list(map(lambda x: x["name"], conditionImmunities))
        tex += f"condition-immunities = {{{', '.join(conditionImmunities)}}},\n"
    if len(senses) > 0:
        tex += f"senses = {{darkvision 60 ft., passive Perception 10}},"
        """
        %saving-throws = {{Str +0, Dex +0, Con +0, Int +0, Wis +0, Cha +0}},
        %skills = {{Acrobatics +0, Animal Handling +0, Arcana +0, Athletics +0, Deception +0, History +0, Insight +0, Intimidation +0, Investigation +0, Medicine +0, Nature +0, Perception +0, Performance +0, Persuasion +0, Religion +0, Sleight of Hand +0, Stealth +0, Survival +0}},
        %damage-vulnerabilities = {{cold}},
        %damage-resistances = {{bludgeoning, piercing, and slashing from nonmagical attacks}},
        %damage-immunities = {{poison}},
        %condition-immunities = {{poisoned}},
        senses = {{darkvision 60 ft., passive Perception 10}},
        languages = {{Common, Goblin, Undercommon}},
        challenge = 1,
      ]"""

    tex += """\\end{{multicols}}
\\end{{DndMonster}}"""

    texContent += "\n\n" + tex

print(texContent)
