# TTRPG-ji v slovenščini
Tukaj tičijo razni ljubiteljski prevodi bolj ali manj znanih namiznih iger vlog in druga podobna šara.

Večina zadev je pisanih v dokumentih tipa Latex, PDF ali Markdown.

**Prevodi niso uradni! Vsa originalna vsebina seveda pripada ustreznim založnikom in avtorjem!**