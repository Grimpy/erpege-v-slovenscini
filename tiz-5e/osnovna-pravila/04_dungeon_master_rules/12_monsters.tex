\chapter{Pošasti}

Kot Gospodar ali Gospodarica temnic imaš na tone sredstev in prijemov, s katerimi lahko popestriš igro igralcem -- in pošasti so na vrhu seznama! Pošast je katerokoli bitje, s katerim se je možno sporazumevati, se z njim boriti in ga ubiti. Po tej definiciji so pošasti celo neškodljive živalce, kakršna je žaba, ali dobrohotna bitja, kot je samorog. Pošasti so tudi ljudje, vilinci, škratje in predstavniki drugih ljudstev, ki bi igranim likom lahko bili prijatelji ali nasprotniki.

V tem poglavju najdeš širok nabor pripravljenih pošasti, ki pa je le košček dolgega seznama iz \textit{Knjige pošasti}. Vsak opis uporablja standarden format, ki jasno opiše zmožnosti pošasti. Spodaj je nekaj smernic za lažje razumevanje informacij v opisih pošasti.

\section{Lastnosti}

Pošastin \textbf{okvirček lastnosti} ponuja bistvene podatke za upravljanje s pošastjo.

\subsection{Velikost}

Pošast je lahko drobna, majhna, povprečna, velika, ogromna ali gromozanska. Za več informacij o velikostih in prostorih bitij glej deveto poglavje.

\subsection{Vrsta}

Vrsta pošasti opredeljuje njeno nrav. Nekateri uroki, čarobni predmeti, razredne značilnosti in drugi učinki v igri vplivajo različno na bitja posamezne vrste. Na primer, \textit{zmajemorna puščica} povzroči dodatno škodo tako zmajem kot tudi drugim bitjem zmajske vrste, kot so zmajske želve in gadjerepi.

Igra predstavlja naslednje vrste pošasti, ki same po sebi nimajo posebnih pravil.

\textbf{Človečnjaki} so glavna ljudstva v TiZ-u. Nekateri so civilizirani, nekateri ne. Sem sodijo ljudje in še vrsta drugih bitij, ki se ponašajo s svojim jeziki in kulturami ter hojo po dveh nogah. Čarobnih sposobnosti človečnjaki nimajo ali pa so le-te zelo skromne (čeprav se večina človečnjakov lahko priuči čaranja). Najobičajnejše človečnjaške rase so tiste, ki so tudi najbolj primerne za igralske like: ljudje, škratje, vilinci in polovnjaki. Prav tako številčni, a mnogo bolj barbarski, krvoločnejši in hudobnejši so goblinjad (goblini, grobgoblini in spaki), orki, gnoli, kuščarjevci in koboldi.

\textbf{Grozošasti} so pošasti v najstrožjem pogledu -- strašljiva, nenavadna bitja, ki niso povsem naravna in skoraj nikoli niso neškodljiva. Nekatera so nastala iz spodletelih poskusov (kot na primer sovji medvedi), druga pa iz strašnih prekletstev (to vključuje minotavre in juan-tije). Zelo težko jih je smiselno kategorizirati, zato lahko za grozošasti označimo vsa bitja, ki ne sodijo nikamor drugam.

\textbf{Konstrukti} se ne rodijo, temveč nastanejo. Nekateri izpolnjujejo preprosta navodila, ki jim jih zapovedo njihovi stvaritelji, drugim pa je podarjena zavest in so zmožni prostega razmišljanja. Najbolj znani konstrukti so golemi. Mnoga bitja, ki živijo na planoti Mehanusa, na primer modroni, so konstrukti, ki so jih ustvarila mogočna bitja iz surovin na planoti.

\textbf{Mračni stvori} so hudobna bitja na nižjih planotah. Nekaj jih služi božanstvom, večini pa gospodujejo vrhovni zlodeji in demonski knezi. Pokvarjeni duhovniki in magi včasih prikličejo mračne stvore v službo na tvarni svet. Medtem ko je zloben nebesnik redkost, je dobroten mračni stvor preprosto nepredstavljiv. Mračni stvori so na primer demoni, zlodeji, peklenski pesjaki, rakšase in jugolothi.

\textbf{Nakazni} so povsem tuja bitja. Mnoga imajo prirojene magične sposobnosti, ki namesto iz mističnih sil sveta izvirajo iz bitjevega tujega uma. Glavni predstavniki nakazni so abolethi, motrila, umovori in slaadi.

\textbf{Nebesniki} domujejo na višjih planotah. Večina jih služi božanstvom kot glasniki ali zastopniki v svetu smrtnikov in na drugih planotah. Po naravi so dobra bitja, zato je take, ki skrenejo na kriva pota, zelo, zelo težko najti. Nebesniki so na primer angeli, kvatli in pegazi.

\textbf{Nemrtvi} so bitja, ki so bila nekoč prej živa, po smrti pa so se zbudila v grozljivo stanje, ki ni ne življenje ne smrt. Vzrok za to je bodisi mračna nekromantika bodisi kako brezbožno prekletstvo. Med nemrtve spadajo hodeča trupla, kot so vampirji ali zombiji, in bitja brez teles, kot so duhovi in prikazni.

\textbf{Prvinci} so bitja, ki živijo na prvinskih planotah. Nekatera so le nekaj več kot žive gmote iz posameznih prvin, vključno z bitji, ki se imenujejo preprosto prvinci. Drugi predstavniki imajo biološka telesa, ki jih prepaja prvinska energija. Najpomembnejše civilizacije na prvinskih planotah tvorijo rase duhcev, na primer džini in efriti. Druga prvinska bitja so še azeri, nevidni zalezovalci in vodne čarošasti.

\textbf{Rastline} so v tem kontekstu rastlinska bitja, ne običajno rastlinje. Večina jih lahko hodi in nekatera so mesojeda. Klasični rastlini sta denimo lazeča gmota in dreant. Rastline so v kontekstu igre tudi plinski trosi, mikonidi in druga glivasta bitja.

\textbf{Velikani} se pnejo visoko v nebo in mečejo senco na preostala ljudstva. So človeške oblike, čeprav imajo nekateri več glav (jotnarji) ali deformacij (fomorji). Velikane delimo na šest vrst: gričevne velikane, kamene velikane, snežne velikane, ognjene velikane, oblačne velikane in nevihtne velikane. Poleg teh so velikani tudi ogri in troli.

\textbf{Vilide} so čarobna bitja, tesno povezana s silami narave. Prebivajo v polmračnih nasadih in mističnih gozdovih. V nekaterih svetovih so tesno povezane z Vilidjo divjino, planoto vil. Najdemo jih tudi na zunanjih planotah, zlasti na planotah Arboreje in Zverinske dežele. Med vilide spadajo driade, škrateljci in satiri.

\textbf{Zmaji} so ogromni starodavni plazilci z veliko močjo. Pravi zmaji, vključno z dobrimi kovinastimi zmaji in hudobnimi barvastimi zmaji, so zelo inteligentni in imajo prirojene magične lastnosti. V to kategorijo spadajo tudi daljni sorodniki pravih zmajev, ki pa so šibkejši, manj inteligentni in manj čarobni, na primer gadjerepi in psevdozmaji. 

\textbf{Zveri} so nečlovečnjaška bitja, ki so del domišljijske favne. Nekatere imajo čarobne moči, a večina jih je neinteligentnih, brez skupnosti ali jezika. Sem sodijo vse sorte običajnih živali, dinozavri in orjaške različice živali.

\textbf{Žlobudre} so želatinasta bitja, ki v glavnem nimajo stalne oblike. Večinoma bivajo v jamah in temnicah pod zemljo in se hranijo z odpadki, mrhovino ali bitji, ki jim po nesreči stopijo na pot. Najbolj znana primerka žlobuder sta črni zdriz in želatinasta kocka.

\subsubsection{Oznake}

Pošast ima lahko ob svoji vrsti v oklepajih tudi eno ali več oznak. Na primer, v orkovem opisu najdemo zapis \textit{človečnjak (ork)}. Te oznake nudijo še dodatno kategorizacijo za nekatera bitja. Oznake same po sebi niso vezane na pravila, lahko pa se nanje nanaša kaj drugega v igri, recimo čaroben predmet. Pomagajmo si s primerom: kopje, ki je posebno učinkovito v boju z demoni, bo ugodno delovalo proti vsem pošastim, ki imajo oznako demona.

\subsection{Opredeljenost}

Opredeljenost pošasti pomaga razumeti njen značaj in obnašanje. Pošast, ki je kaotična in zla, ne bo poslušala glasu razuma in bo brez pomisleka napadla vsako bitje, ki ga bo zagledala, nevtralna pošast pa se bo morda pripravljena pogovoriti. Več o opredeljenosti piše v četrtem poglavju.

Opredeljenost, zapisana v okvirčku lastnosti, je privzeta opredeljenost pošasti, ki pa ni fiksna. Lahko jo spremeniš in prilagodiš, kot najbolje ustreza tvoji kampanji. Nič ti ne brani, da pred igralce ne postaviš dobrotljive zelene zmajevke ali hudobnega nevihtnega velikana.

Nekatera bitja imajo lahko \textbf{poljubno opredeljenost}. Z drugimi besedami, sam/a določiš opredeljenost pošasti. Pri nekaterih pošastih je ob opredeljenosti namignjeno, ali se pošast nagiba bolj proti poštenosti, kaosu, dobremu ali zlu. Na primer, berserker je lahko kakršnekoli kaotične opredeljenosti (kaotičen in dober, kaotičen in nevtralen ali kaotičen in zel).

Mnoga neinteligentna bitja ne poznajo konceptov zakonja in kaosa, dobrega in zlega. Ne sprejemajo moralnih ali etičnih odločitev, temveč se ravnajo po čutih. Ta bitja so \textbf{neopredeljena}.

\subsection{Zaščitni razred}

Pošast, ki nosi oklep ali ščit, si zaščitni razred (ZR) izračuna iz oklepa, ščita in Gibčnosti. Drugače pa je njen ZR osnovan na popravku Gibčnosti in morebitnem naravnem oklepu. Če ima pošast naravni oklep ali nosi oklep ali ščit, je to zabeleženo v oklepajih poleg vrednost ZR-ja.

\subsection{Življenjske točke}

Pošast umre, ko izgubi vse življenjske točke. Več o življenjskih točkah piše v prvem poglavju.

Življenjske točke pošasti so predstavljene kot izračuni kock in kot povprečno število. Na primer, pošast z 2k8 življenjskimi točkami ima v povprečju 9 življenjskih točk (2 $\times$ 4,5).

Velikostni razred pošasti določa kocko, s katero izračunamo njene življenjske točke. To je prikazano v spodnji preglednici.

\begin{DndTable}[header = Življenjske kocke glede na velikostni razred]{Xcc}
    \textbf{Velikost pošasti} & \textbf{Živ. kocka} & \makecell{\textbf{Povprečne živ. točke}\\\textbf{na kocko}} \\
    Drobna & k4 & 2,5 \\
    Majhna & k6 & 3,5 \\
    Povprečna & k8 & 4,5 \\
    Velika & k10 & 5,5 \\
    Ogromna & k12 & 6,5 \\
    Gromozanska & k20 & 10,5 
\end{DndTable}

Pošastin popravek Konstitucije prav tako vpliva na njene življenjske točke. Njen popravek Konstitucije pomnožimo s številom življenjskih kock, rezultat pa nato prištejemo življenjskim točkam. Na primer, če ima pošast oceno Konstitucije 12 (popravek +1) in 2k8 življenjski kocki, ima na koncu 2k8 + 2 življenjskih točk (v povprečju 11).

\subsection{Hitrost}

Hitrost pošasti ti pove, kako daleč se lahko premakne v eni potezi. Več o hitrosti piše v osmem in devetem poglavju.

Vsa bitja imajo hitrost hoje, ki se poenostavljeno imenuje pošastina hitrost. Bitja, ki se ne morejo premikati po tleh, imajo hitrost hoje 0 metrov.

Nekatera bitja imajo enega ali več dodatnih načinov premikanja. Ti so opisani spodaj.

\subsubsection{Kopánje}

Pošast, ki ima hitrost kopánja, se lahko s to hitrostjo prekoplje skozi pesek, zemljo, blato ali led. Pošast ne more kopati skozi kamen, razen če ima posebno lastnost, ki ji to omogoča.

\subsubsection{Letenje}

Pošast, ki ima hitrost letenja, lahko leti. Nekatera bitja imajo sposobnost \textbf{lebdenja}, zato jih je težje sklatiti iz zraka (to je razloženo v devetem poglavju). Pošast ob svoji smrti neha lebdeti.

\subsubsection{Plavanje}

Pošasti s hitrostjo plavanja ni potrebno zapraviti dodatnega premikanja za plavanje.

\subsubsection{Plezanje}

Pošast, ki ima hitrost plezanja, se lahko premika po navpičnih površinah. Za plezanje ji ni potrebno zapravljati dodatnega premikanja.

\subsection{Ocene sposobnosti}

Vsaka pošast ima šest ocen sposobnosti (Moč, Gibčnost, Konstitucijo, Inteligenco, Modrost in Karizmo) in pripadajoče popravke. Več o ocenah sposobnosti in njihovi uporabi najdeš v sedmem poglavju.

\subsection{Rešilni meti}

Polje z oznako Rešilni meti je navedeno pri bitjih, ki se spretno upirajo nekaterim učinkom. Bitje, ki ga ni lahko očarati ali prestrašiti, na primer, lahko dobi prištevek na svoje rešilne mete Modrosti. Večina bitij nima posebnih prištevkov za rešilne mete, zato tega polja v njihovem okvirčku lastnosti ni.

Prištevek za rešilni met je seštevek pošastinega ustreznega popravka sposobnosti in njenega izvedenskega prištevka, ki ga določa pošastina ocena izziva (glej preglednico Izvedenski prištevek po oceni izziva).

\begin{DndTable}[header = Izvedenski prištevek po oceni izziva]{ccXcc}
    \makecell{\textbf{Ocena}\\\textbf{izziva}} & \makecell{\textbf{Izvedenski}\\\textbf{prištevek}} & & \makecell{\textbf{Ocena}\\\textbf{izziva}} & \makecell{\textbf{Izvedenski}\\\textbf{prištevek}} \\
    0 & +2 & & 14 & +5 \\
    1/8 & +2 & & 15 & +5 \\
    1/4 & +2 & & 16 & +5 \\
    1/2 & +2 & & 17 & +6 \\
    1 & +2 & & 18 & +6 \\
    2 & +2 & & 19 & +6 \\
    3 & +2 & & 20 & +6 \\
    4 & +2 & & 21 & +7 \\
    5 & +3 & & 22 & +7 \\
    6 & +3 & & 23 & +7 \\
    7 & +3 & & 24 & +7 \\
    8 & +3 & & 25 & +8 \\
    9 & +4 & & 26 & +8 \\
    10 & +4 & & 27 & +8 \\
    11 & +4 & & 28 & +8 \\
    12 & +4 & & 29 & +9 \\
    13 & +5 & & 30 & +9
\end{DndTable}

\subsection{Veščine}

Polje z oznako Veščine je rezervirano za pošasti, ki so izvedene v eni ali več veščinah. Na primer, pošast, ki zelo dobro zaznava okolico in se odlično tihotapi, ima prištevek na svoje preizkuse Modrosti (Zaznavanje) in Gibčnosti (Prikritost).

Veščinski prištevek je vsota pošastinega ustreznega popravka sposobnosti in njenega izvedenskega prištevka, ki ga določa pošastina ocena izziva (glej preglednico Izvedenski prištevek po oceni izziva). Na veščinski priševek lahko vplivajo tudi drugi parametri. Kakšna pošast ima lahko denimo nepričakovano večji prištevek (običajno dvakratnik svojega izvedenskega prištevka), ker se na neko stvar neverjetno dobro spozna.

\subsection{Občutljivosti, odpornosti in imunosti}

Nekatera bitja so občutljiva, odporna ali imuna na določene tipe škode. Določena bitja so celo odporna ali imuna na škodo, ki jo povzročijo nemagični napadi (magični napad je napad, ki ga izvedemo s pomočjo uroka, čarobnega predmeta ali drugega izvora magije). Poleg tega so nekatera bitja imuna na določena stanja.

\subsection{Čuti}

Polje Čuti vsebuje oceno pošastine pasivne Modrosti (Zaznavanje) in kakršnekoli morebitne posebne čute, ki jih pošast ima. Posebni čuti so opisani spodaj.

\subsubsection{Slepovid}

Slepogleda pošast lahko zazna svojo okolico v omejenem dosegu brez uporabe vida.

Bitja brez oči, kot so na primer grdavsi ali sive žlobudre, in bitja z eholokacijo ali izostrenimi čuti, denimo netopirji ali pravi zmaji, večinoma imajo ta poseben čut.

Če je pošast privzeto slepa, ima poleg slepovida v oklepajih pripis, ki označuje, da doseg njenega slepovida hkrati predstavlja skrajni domet njenega zaznavanja okolice.

\subsubsection{Temovid}

Temogleda pošast lahko vidi v temi v določenem dosegu. Pošast lahko v medli svetlobi v dosegu vidi tako jasno kakor v močni svetlobi, v temi pa tako jasno kakor v medli svetlobi. V temi pošast ne more razločiti barv, le odtenke sivine. Ta poseben čut imajo mnoga bitja, ki živijo pod zemljo.

\subsubsection{Tresovid}

Tresogleda pošast lahko zazna in določi izvor vibracij v določenem dosegu. Pošast in izvor se morata dotikati iste površine ali substance. S tresovidom ni mogoče zaznati letečih bitij ali bitij brez telesa. Mnoga podzemna bitja, kot so ankhegi in temnorjavi okorniki, imajo ta poseben čut.

\subsubsection{Vsevid}

Vsegleda pošast lahko v določenem dosegu vidi v normalni in magični temi, vidi nevidna bitja in predmete, samodejno zazna privide in uspešno opravi rešilne mete proti njim ter vidi prvotno obliko preobražencev ali bitij, ki jim je magija spremenila podobo. Poleg tega lahko pošast vidi na Etrno planoto v enakem dosegu.

\subsection{Jeziki}

Jeziki, ki jih pošast govori, so znizani v abecednem vrstnem redu. Včasih lahko pošast razume kak jezik, vendar ga ne govori, kar je tudi ustrezno označeno. Znak \textquote{--} pomeni, da bitje niti ne govori niti ne razume nobenega jezika.

\subsubsection{Telepatija}

Telepatija je magična sposobnost, ki pošasti omogoča, da prek misli komunicira z drugim bitjem v določenem dosegu. Bitju, ki ga pošast ogovori telepatsko, ni treba imeti nobenega skupnega jezika s pošastjo, mora pa razumeti vsaj en poljuben jezik. Bitje brez telepatske sposobnosti lahko prejema telepatska sporočila in se nanje odziva, vendar ne more začeti ali končati telepatskega pogovora.

Pošasti s telepatijo ni treba videti sogovorca in lahko telepatsko zvezo prekine kadarkoli. Zveza se prekine takoj, ko sta sogovorca predaleč narazen ali ko telepatska pošast začne pogovor z drugim bitjem. Telepatska pošast lahko prične ali konča telepatski pogovor, ne da bi porabila dejanje, a če je pošast onesposobljena, ne more vzpostavljati telepatskih povezav ali vzdrževati obstoječih.

Bitje v območju \textit{protimagičnega polja} ali na mestu, kjer magija ne deluje, ne more pošiljati ali prejemati telepatskih sporočil.

\subsection{Izziv}

Pošastina \textbf{ocena izziva} pove, kolikšno grožnjo pošast predstavlja po merilih v trinajstem poglavju. Ta merila vključujejo število pustolovcev določene stopnje, ki naj bi bili sposobni premagati pošast z določeno oceno izziva, ne da bi kateri od pustolovcev pri tem umrl.

Pošasti, ki so precej šibkejše od prvostopenjskih likov, imajo oceno izziva nižjo od 1. Pošasti z oceno izziva 0 so neškodljive, razen v večjih skupinah; tiste brez učinkovitih napadalnih zmožnosti ne prinesejo nobenih izkušenjskih točk, tiste z pa vsaka po 10 IT.

Nekaterim pošastim ni kos niti tipična skupina pustolovcev 20. stopnje. Te pošasti imajo oceno izziva 21 ali več in so namenjene temu, da igralce priženejo do skrajnih meja.

\subsubsection{Izkušenjske točke}

Koliko izkušenjskih točk (IT) je vredna pošast, je odvisno od ocene izziva. Običajno IT dobiš, ko premagaš pošast, vendar lahko Gospodar temnic podari IT tudi v primeru, da grožnjo, ki jo je pošast predstavljala, izničiš na kak drug način.

Če ni rečeno drugače, je pošast, ki jo prikliče urok ali magična sposobnost, vredna toliko IT, kot piše v njenem okvirčku lastnosti.

V trinajstem poglavju je razloženo, kako sestaviti nalete v mejah ciljne količine IT in kako prilagoditi težavnost naleta.

\begin{DndTable}[header = Izkušenjske točke po oceni izziva]{ccXcc}
    \textbf{Ocena izziva} & \textbf{IT} & & \textbf{Ocena izziva} & \textbf{IT} \\
    0 & 0 ali 10 & & 14 & 11.500 \\
    1/8 & 25 & & 15 & 13.000 \\
    1/4 & 50 & & 16 & 15.000 \\
    1/2 & 100 & & 17 & 18.000 \\
    1 & 200 & & 18 & 20.000 \\
    2 & 450 & & 19 & 22.000 \\
    3 & 700 & & 20 & 25.000 \\
    4 & 1.100 & & 21 & 33.000 \\
    5 & 1.800 & & 22 & 41.000 \\
    6 & 2.300 & & 23 & 50.000 \\
    7 & 2.900 & & 24 & 62.000 \\
    8 & 3.900 & & 25 & 75.000 \\
    9 & 5.000 & & 26 & 90.000 \\
    10 & 5.900 & & 27 & 105.000 \\
    11 & 7.200 & & 28 & 120.000 \\
    12 & 8.400 & & 29 & 135.000 \\
    13 & 10.000 & & 30 & 155.000
\end{DndTable}

\subsection{Posebne lastnosti}

Posebne lastnosti (ki jih najdemo med pošastino oceno izziva in dejanji ali odzivi) so karakteristike, ki bodo najverjetneje pomembne med spopadom in ki zahtevajo dodatno razlago.

\subsubsection{Prirojeno čaranje}

Pošast s prirojeno sposobnostjo čaranja urokov ima lastnost Prirojeno čaranje. Če ni drugače označeno, se prirojeni urok prve ali višje ravni vedno izvede na najnižji možni ravni in ga ni mogoče vršiti na višji ravni. Če ima pošast kakšen čar, ki je odvisen od njene stopnje in le-ta ni podana, uporabi kar pošastino oceno izziva.

Prirojeni urok ima lahko posebna pravila ali omejitve. Na primer, droški mag lahko prirojeno vrši urok \textit{lebdenja}, vendar ima urok omejitev \textquote{na sebi}, kar pomeni, da urok vedno učinkuje le na droškega maga.

Prirojenih urokov pošasti ni mogoče zamenjati z drugimi uroki. Če prirojeni uroki ne zahtevajo napadalnih metov, jim ne prišteješ prištevka k napadu.

\subsubsection{Čaranje}

Pošast z lastnostjo čaranja ima uročevalsko raven in uročna polja, ki jih uporablja za vršenje urokov prve ali višje ravni (kot je razloženo v desetem poglavju). Uročevalska raven se uporablja tudi za kakršnekoli čare, ki so všteti v lastnost uročevanja.

Pošast ima seznam poznanih ali pripravljenih urokov, ki jih črpa iz določenega razreda. Seznam lahko vsebuje tudi uroke iz lastnosti tega razreda (kot primer navajamo klerikovo lastnost Božje domene). Pošast obravnavamo kot pripadnika nekega razreda, ko se združi z ali uporablja magični predmet, ki zahteva članstvo v tem razredu ali dostop do njegovega seznama urokov.

Pošast lahko izvede urok iz svojega seznama na višji ravni, če ima za to ustrezno uročno polje. Za primer se vrnimo k droškemu magu, ki ima urok \textit{čarobni blisk} tretje ravni. Ta urok lahko vrši kot urok pete ravni tako, da porabi enega izmed uročnih polj pete ravni.

Uroke, ki jih pošast pozna ali jih ima pripravljene, lahko spremeniš in zamenjaš katerikoli urok na pošastinem seznamu urokov z drugim urokom iste ravni in iz istega seznama urokov. Če zamenjaš uroke, lahko postane pošast manj ali bolj nevarna, kot bi sklepali iz njene ocene izziva.

\subsection{Dejanja}

Ko pošast pride na vrsto za svoje dejanje, lahko izbira med opcijami iz razdelka Dejanja v njenem okvirčku lastnosti ali pa izvede eno od dejanj, ki so na voljo vsem bitjem, na primer Nagel tek ali Skrivanje (več o tem v devetem poglavju).

\subsubsection{Bližnji in oddaljeni napadi}

Najobičajnejša dejanja, ki jih bo pošast izvedla v spopadu, so bližnji in oddaljeni napadi. Ti so lahko bodisi uročni bodisi oboroženi napadi, pri čemer je \textquote{orožje} lahko izdelano ali naravno(na primer krempelj ali bodica na repu). Več o različnih vrstah napadov piše v devetem poglavju.

\subparagraph{Bitje ali tarča}
Tarča napada je običajno eno bitje ali ena tarča. Razlika je v tem, da je lahko \textquote{tarča} poleg bitja tudi predmet.

\subparagraph{Uspeh}
Škoda in učinki, ki jih povzroči napad, ki zadane tarčo, so opisani za oznako \textquote{Uspeh:}. Možnost imaš uporabiti povprečno škodo ali metati kocke za škodo; iz tega razloga je zapisano oboje, povprečna škoda in formula s kockami.

\subparagraph{Neuspeh}
Če ima napad učinek, ki se pojavi ob neuspešnem napadu, to piše za oznako \textquote{Neuspeh:}.

\subsubsection{Več napadov}

Bitje s to sposobnostjo lahko med svojo potezo izvede več napadov. Bitje ne more izvesti več napadov, kadar izvede priložnosti napad, ki mora biti en sam bližnji napad.

\begin{DndSidebar}{Pravila primeža za pošasti}
    Veliko pošasti ima posebne napade, ki jim omogočajo, da uporabijo primež na svojem plenu. Kadar pošast izvede takšen napad, ji ni treba narediti dodatnega preizkusa sposobnosti za uspešnost primeža, razen če napad narekuje drugače.

    Bitje, ki ga ima pošast v primežu, lahko porabi svoje dejanje za poskus izvitja iz primeža. Uspeti mu mora preizkus Moči (Atletika) ali Gibčnosti (Akrobatika) s TR, ki je zapisan v pošastinem okvirčku lastnosti. Če TR ni zapisan, predpostavi, da je TR 10 + pošastin popravek Moči (Atletika).
\end{DndSidebar}

\subsubsection{Strelivo}

Pošast s seboj nosi dovolj streliva, da lahko napada iz daljave. Predvidevaš lahko, da ima pošast 2k4 kosov streliva za napad z zalučanim orožjem in 2k10 kosov streliva za strelno orožje, kakršna sta denimo lok ali samostrel.

\subsection{Odzivi}

Če lahko pošast s svojim odzivom stori kaj posebnega, o tem piše tukaj. Če bitje nima posebnega odziva, tega razdelka v njegovem okvirčku ni.

\subsection{Omejena uporaba}

Nekatere posebne sposobnosti imajo omejeno število uporab.

\subparagraph{X/dan}
Notacija \textquote{X/dan} pomeni, da je posebno sposobnost mogoče uporabiti nekajkrat, po tem pa mora pošast opraviti daljši počitek, da se ji porabljene porabe vrnejo. Na primer, \textquote{1/dan} pomeni, da je posebno sposobnost mogoče uporabiti enkrat, po tem pa mora pošast opraviti daljši počitek, preden lahko sposobnost spet uporabi.

\subparagraph{Obnovitev X-Y}
Notacija \textquote{Obnovitev X-Y} pomeni, da lahko pošast to posebno sposobnost uporabi enkrat, potem pa se sposobnost naključno obnovi tekom spopada. Na začetku vsake pošastine poteze vrzi k6. Če je izid število X ali Y, pošast lahko spet uporabi posebno sposobnost. Sposobnost se povrne tudi, ko pošast konča krajši ali daljši počitek.

Poglejmo primer: \textquote{Obnovitev 5-6} pomeni, da lahko pošast sposobnost uporabi enkrat. Potem je na začetku vsake svoje naslednje poteze upravičena do ponovne uporabe, če vrže k6 in je izid 5 ali 6.

\subparagraph{Obnovitev po krajšem ali daljšem počitku}
Ta notacija pomeni, da pošast lahko uporabi posebno sposobnost enkrat, potem pa mora pred naslednjo uporabo opraviti krajši ali daljši počitek.

\subsection{Oprema}

Okvirček lastnosti redko navaja opremo, ki ni oklep ali orožje. Ker se od bitij, kot so na primer človečnjaki, tako ali tako pričakuje, da nosijo primerna oblačila, le-ta običajno niso navedena.

Pošastim lahko po želji oprtaš razno prtljago in opremo, pri čemer si lahko pomagaš s petim poglavjem. Sam/a odločiš tudi, koliko pošastine opreme pustolovci najdejo, potem ko je bila pošast ubita, in ali je ta oprema še uporabna. Zdelan oklep, narejen posebej za pošast, je le redko primeren ali uporaben za koga drugega.

Če čarajoča pošast potrebuje sestavine za svoje uroke, lahko predpostavljaš, da jih ima, čeprav niso navedene v okvirčku lastnosti.

\section{Legendarna bitja}

Legendarno bitje je zmožno stvari, ki jih običajna bitja ne zmorejo. Z magijo lahko vpliva na okolico tudi več kilometrov proč, in tudi ko ni na vrsti, lahko izvaja posebna dejanja.

Če se bitje prelevi v legendarno bitje, na primer s pomočjo uroka, vseeno ne more izvajati legendarnih dejanj, dejanj v povezavi z brlogom ali območnih učinkov tega bitja.

\subsection{Legendarna dejanja}

Legendarno bitje lahko izvede določeno število posebnih dejanj -- pravimo jim legendarna dejanja --, ko ni na vrsti. Naenkrat lahko porabi le eno posebno dejanje in le ob koncu dejanja drugega bitja. Bitje si povrne vsa svoja porabljena legendarna dejanja na začetku svoje naslednje poteze. Ni nujno, da bitje sploh izvede kakšno legendarno dejanje, če tega ne želi. Legendarnih dejanj ni mogoče izvajati, dokler je bitje onesposobljeno ali kako drugače nesposobno izvajati dejanja. Če je bitje presenečeno, legendarnih dejanj ne more koristiti, dokler ne konča svoje prve poteze v spopadu.

\subsection{Brlog legendarnega bitja}

V okvirčku legendarnega bitja lahko piše tudi kaj o njegovem brlogu in o posebnih učinkih, ki jih lahko ustvarja v njem, bodisi z močjo volje ali le s svojo navzočnostjo. Nimajo vsa legendarna bitja brlogov. Pravila za brlog veljajo le za legendarna bitja, ki preživijo ogromno časa v svojih brlogih in jih tam najpogosteje tudi najdemo.

\subsubsection{Dejanja v brlogu}

Če ima legendarno bitje na voljo dejanja, ki se nanašajo na brlog, lahko z njimi črpa okoljsko magijo iz svojega brloga. Brlog v tem primeru prejme svojo lastno odzivnost v spopadu, ki je privzeto 20 (če ima še kak drug lik odzivnost 20, se samodejno uvrsti višje od brloga). Kadar je brlog na vrsti v spopadu, se lahko legendarno bitje odloči, ali bo izvedlo kakšno dejanje brloga, razen če je bitje onesposobljeno ali kako drugače nesposobno opravljati dejanja. Če je bitje presenečeno, dejanj v povezavi z brlogom ne more koristiti, dokler ne končna svoje prve poteze v spopadu.

\subsubsection{Območni učinki}

Že sama navzočnost legendarnega bitja lahko čudno in čarobno vpliva na okolico, kar je zapisano v tem razdelku. Ko legendarno bitje umre, se območni učinki v hipu razblinijo ali postopoma popustijo.

\section{Stranske osebe}

V tem odseku so navedene lastnosti raznih človečnjaških neigranih stranskih oseb, na katere bi pustolovci lahko naleteli med povprečno kampanjo. S temi okvirčki lastnosti je možno predstaviti tako človeške kot nečloveške stranske osebe.

\subsection{Prirejanje stranskih oseb}

Stranske osebe je mogoče prirediti po svojih potrebah na več načinov.

\subparagraph{Rasne lastnosti}
Stranski osebi lahko dodaš rasne lastnosti. Na primer, polovnjaški svečenik ima lahko hitrost 7,5 metrov in lastnost S srečo v žepu. Dodajanje rasnih lastnosti ne spremeni ocene izziva stranske osebe. Več o rasnih lastnostih piše v drugem poglavju.

\subparagraph{Zamenjave urokov}
Če je stranska oseba uročevalec, lahko zamenjaš enega ali več njegovih urokov z drugimi. Kateregakoli izmed urokov na osebinem seznamu lahko zamenjaš z drugim urokom iste ravni in iz istega uročnega seznama. Takšne zamenjave ne spremenijo ocene izziva stranske osebe.

\subparagraph{Zamenjave oklepa in orožij}
Lahko okrepiš ali oslabiš osebin oklep in zamenjaš njegova orožja. Take prilagoditve lahko spremenijo oceno izziva stranske osebe.

\subparagraph{Magični predmeti}
Mogočnejša je oseba, tem verjetneje je, da ima pri sebi kakšno čarobno reč. Mag ima morda čarobno palico in/ali enega ali več napojev ter zvitkov. Če osebi podeliš dovolj močan magični predmet, ki lahko škoduje drugim, to lahko spremeni njegovo oceno izziva.

Nekaj magičnih predmetov je opisanih v štirinajstem poglavju.

\newpage

\input{04_dungeon_master_rules/monster_blocks.tex}

\clearpage

\input{04_dungeon_master_rules/NPCs.tex}

\clearpage
\newpage