\chapter{Vodenje igre}

Tukaj so zapisana pravila, ki bodo Gospodarjem temnic pomagala voditi igro. Za več informacij si preberi \textit{Vodič Gospodarja temnic}.

\section{Predmeti}

Kadar morajo liki prerezati vrvi, razbiti okno ali razsekati vampirjevo krsto, je edino pomembno pravilo naslednje: če imajo dovolj časa in pravo orodje, lahko liki uničijo praktično vsak uničljiv predmet.

Ko ugotavljaš, ali likom uspe poškodovati predmet, uporabi kmečko pamet. Ali lahko borec zareže skozi kamniti zid z mečem? Ne, najbrž se bo meč uničil prej kot zid.

Kar se teh pravil tiče, je predmet samostojna, neživa stvar, na primer okno, vrata, meč, knjiga, miza, stol ali kamen, ne pa tudi stavba ali vozilo, ki sta sestavljena iz več različnih predmetov.

\subsection{Lastnosti predmetov}

Uničljivim predmetov lahko dodeliš zaščitni razred in življenjske točke, imunosti, odpornosti in občutljivosti za določene vrste škode.

\subsubsection{Zaščitni razred}

Zaščitni razred predmeta pove, kako težko ga je poškodovati, ko ga udarimo (predmet se namreč ne more izmakniti udarcu). Spodnja preglednica prikazuje predlagane zaščitne razrede za razne snovi.

\begin{DndTable}{Xc}
    \textbf{Snov} & \textbf{ZR} \\
    Blago, papir, vrv & 11 \\
    Kristal, steklo, led & 13 \\
    Les, kost & 15 \\
    Kamen & 17 \\
    Železo, jeklo & 19 \\
    Mithral & 21 \\
    Adamant & 23
\end{DndTable}

\subsubsection{Življenjske točke}

Življenjske točke predmeta povedo, koliko poškodb lahko prenese, preden izgubi trdnost. Prožnejši in vzdržljivejši predmeti imajo več življenjskih točk od bolj krhkih. Tudi večji predmeti imajo običajno več življenjskih točk od manjših predmetov, razen če je uničenje manjšega dela predmeta prav tako učinkovito kot uničenje celote. Spodnja preglednica ponuja nekaj predlogov življenjskih točk za razne vrste predmetov.

\begin{DndTable}{Xcc}
    \textbf{Velikost} & \textbf{Krhek} & \textbf{Odporen} \\
    Droben (steklenica, ključavnica) & 2 (1k4) & 5 (2k4) \\
    Majhen (skrinja, lutnja) & 3 (1k6) & 10 (3k6) \\
    Povprečen (sod, lestenec) & 4 (1k8) & 18 (4k8) \\
    Velik (ciza, okno površine 3x3 m) & 5 (1k10) & 27 (5k10)
\end{DndTable}

\subsubsection{Ogromni in gromozanski predmeti}

Če hočemo poškodovati ogromne ali gromozanske predmete, kot so orjaški spomenik, visok kamnit obelisk ali masivna skala, si ne moremo pomagati z običajnimi orožji. Kljub temu lahko že ena bakla zažge ogromen gobelin in urok potres lahko orjaški kip sesuje v prah. Če želiš, si lahko zapisuješ in spremljaš življenjske točke ogromnih in gromozanskih predmetov, ali pa se preprosto odločiš, kako dolgo bo predmet klonil pod silo, ki nanj vpliva. Če se odločiš za spremljanje življenjskih točk, razdeli predmet na velike ali manjše odseke ter določi življenjske točke vsakemu posebej. Uničenje enega izmed teh odsekov lahko sesuje ves predmet. Na primer, gromozanski človeški kip se lahko zruši že, če ena od njegovih velikih nog izgubi vse življenjske točke.

\subsubsection{Predmeti in vrste škode}

Predmeti so imuni na strupe in poškodbe uma. Lahko se odločiš tudi, da so nekatere vrste škode pri določenih predmetih in snoveh bolj učinkovite od drugih. Na primer, s topim udarcem se da veliko doseči, ko razbijamo stvari, ne pa toliko, ko sekamo vrvi ali lestve. Predmeti iz papirja ali blaga so lahko še posebej občutljivi na ogenj ali plazmo. Kramp je učinkovit pri razbijanju kamna, ne pa toliko pri podiranju dreves. Kot zmeraj se odloči tako, da se ti bo zdelo najbolj prav.

\subsubsection{Poškodbeni prag}

Večji predmeti, kot so recimo grajski zidovi, imajo pogosto dodatno odpornost, ki jo predstavlja poškodbeni prag. Predmet s poškodbenim pragom je imun na vso škodo, razen če od enega napada utrpi toliko točk škode, da je njihova količina večja ali enaka njegovemu poškodbenemu pragu. V tem primeru normalno utrpi škodo. Vsa škoda, ki ne doseže ali preseže poškodbenega praga, se smatra za zanemarljivo in ne vpliva na število življenjskih točk napadenega predmeta.

\section{Pasti}

Pasti je mogoče najti skoraj kjerkoli. En nepreviden korak v starodavni grobnici lahko sproži mehanizem vrtečih se rezil, ki te sesekljajo z oklepom vred. Navidez nedolžne vitice nad vhodom v jamo lahko zavijejo in zadušijo vsakogar, ki se rine skoznje. Mreža, ki je skrita v drevesnih krošnjah, lahko pade na pustolovce, ki hodijo spodaj. V domišljijski igri lahko nepazljivi pustolovci padejo v smrt, živi zgorijo ali izdihnejo zaradi zastrupljenih puščic.

Past lahko deluje na mehanizem ali na magijo. \textbf{Mehanske pasti} vključujejo brezna, pasti s strelicami, padajoče klade, sobe, ki se polnijo z vodo, vrteča rezila in karkoli drugega, kar deluje na mehanizem. \textbf{Magične pasti} se delijo dalje na pasti na magično napravo in uročne pasti. Pasti na magično napravo izvedejo uročne učinke, ko jih sprožimo. Uročne pasti so uroki, recimo \textit{preprečitveno znamenje} in \textit{simbol}, ki delujejo kot pasti.

\subsection{Pasti med igro}

Ko pustolovci naletijo na past, moraš kot GT vedeti, kako se past sproži, kaj počne, ali obstaja možnost, da jo liki pravočasno opazijo, in kako jo onesposobiti ali obiti.

\subsubsection{Sprožitev pasti}

Večina pasti se sproži, ko bitje nekam stopi ali se dotakne nečesa, kar je stvaritelj pasti hotel zaščititi. Nekaj najobičajnejših sprožil je stopanje na sprožilno ploščad, spotik čez potezno žico, obračanje vratne kljuke ali odklepanje zapaha z napačnim ključem. Magične pasti se navadno sprožijo, ko bitje vstopi v prostor ali se dotakne predmeta. Nekatere (na primer urok \textit{preprečitveno znamenje}) imajo zapletenejše sprožitvene pogoje, denimo geslo, ki prepreči sprožitev.

\subsubsection{Zaznavanje in onesposobljanje pasti}

Pogosto je ob podrobnejšem pregledu mogoče videti kak del pasti. Liki nemara zasledijo dvignjen ali neporavnan tlakovec, ki skriva sprožilno ploščad, opazijo odblesk svetlobe od tanke napete žice, zagledajo majhne luknje v steni, iz katerih bodo bruhnili plameni, ali drugače zaznajo nekaj, kar kaže na prisotnost pasti.

Opisi pasti določajo preizkuse in TR-je za njeno odkritje, onesposobitev ali oboje. Lik, ki aktivno išče past, lahko izvede preizkus Modrosti (Zaznavanje) s TR-jem pasti. Poleg tega lahko primerjaš TR za zaznavo pasti s pasivno Modrostjo (Zaznavanje) vsakega lika in tako določiš, ali kateri izmed članov druščine mimogrede opazi past. Če pustolovci zaznajo past, preden jo sprožijo, jo lahko mogoče onesposobijo trajno ali za vsaj tako dolgo, da se lahko prebijejo mimo. Da lik ugotovi, kako past onemogočiti, mu lahko dosodiš preizkus Inteligence (Preiskovanje), potem pa še preizkus Gibčnosti s tatinskim orodjem za samo izvedbo sabotaže.

Katerikoli lik lahko poskusi zaznati ali onemogočiti magično past, bodisi s preizkusom Inteligence (Čudoslovje) bodisi z morebitnimi drugimi preizkusi, ki jih omenja opis pasti. TR je enak ne glede na preizkus. Povrhu je možno z urokom \textit{razuročevanje} odpraviti večino magičnih pasti. Opis magične pasti beleži TR za preizkus sposobnosti, ko uporabiš urok \textit{razuročevanje}.

V večini primerov je opis pasti dovolj jasen, da lahko brez težav presodiš, ali likovo prizadevanje privede do odkritja ali onesposobitve pasti. Kot pri večini igre tudi tukaj ne dajaj prednosti metanju kock pred pametno igro in dobrim načrtovanjem. Uporabi dobro kmečko pamet in iz opisa pasti odloči, kaj se zgodi.

Če lik stori nekaj, kar bo brez dvoma razkrilo prisotnost pasti, mu dovoli, da odkrije past tudi brez preizkusa sposobnosti. Na primer, če pustolovec dvigne preprogo, ki skriva sprožilno ploščad, mu je uspelo najti sprožilec brez dodatnih preizkusov.

Razorožitev past je lahko nekoliko zahtevnejša. Poglejmo si primer pasti v skrinji z zakladom. Če skrinjo odpremo, ne da bi prej potisnili dveh ročajev na straneh skrinje, mehanizem v notranjosti izstreli plaz zastrupljenih igel proti komurkoli pred skrinjo. Po pregledu skrinje in nekaj preizkusih sposobnosti liki še zmeraj niso sigurni, ali je v skrinji past. Namesto da jo na vrat na nos odprejo, postavijo prednjo ščit in z železno palico odprejo skrinjo na daljavo. Mehanizem se še vedno sproži, vendar igle trčijo ob ščit in vsi ostanejo živi in zdravi.

Pasti imajo pogosto mehanizme, prek katerih jih je mogoče onemogočiti ali zaobiti. Inteligentne pošasti, ki skrivajo pasti po svojih brlogih, morajo imeti način, da jih prečkajo, ne da bi se še same poškodovale. Takšne pasti imajo lahko skrite vzvode, ki jih onemogočijo, ali skrivne prehode, ki past obidejo.

\subsubsection{Posledice pasti}

Posledice pasti so lahko vse od rahlo neprijetnih do smrtnih. Pri tem si pomagajo s puščicami, bodicami, rezili, strupom, plini, ognjem in globokimi brezni. Najbolj smrtonosne pasti združujejo več teh elementov. Opis pasti pove, kaj se zgodi, ko past sprožimo.

Napadalni prištevek pasti, rešilni TR proti njenim posledicam in poškodbe, ki jih past povzroči, se lahko razlikujejo glede na njeno tveganost. Če želiš nekaj priporočil za določanje napadalnega prištevka, rešilnega TR in škode glede na tveganost pasti, si lahko pomagaš s spodnjimi preglednicami.

\textbf{Zaviralna past} naj ne bi ubila ali resno poškodovala pustolovcev na izbranih stopnjah, medtem ko je namen \textbf{nevarne pasti} resno ogroziti (ali celo ubiti) tostopenjske like. \textbf{Smrtonosna past} bo najverjetneje ubila like na določenih stopnjah.

\begin{DndTable}[header=Rešilni TR-ji in napadalni prištevki]{Xcc}
    \textbf{Tveganost pasti} & \textbf{Rešilni TR} & \textbf{Napadalni prištevek} \\
    Zaviralna & 10 - 11 & +3 do +5 \\
    Nevarna & 12 - 15 & +6 do +8 \\
    Smrtonosna & 16 - 20 & +9 do +12
\end{DndTable}

\begin{DndTable}[header=Resnost poškodb glede na stopnjo]{Xccc}
    \textbf{Stopnja lika} & \textbf{Zaviralna} & \textbf{Nevarna} & \textbf{Smrtonosna} \\
    1 - 4 & 1k10 & 2k10 & 4k10 \\
    5 - 10 & 2k10 & 4k10 & 10k10 \\
    11 - 16 & 4k10 & 10k10 & 18k10 \\
    17 - 20 & 10k10 & 18k10 & 24k10
\end{DndTable}

\subsubsection{Zapletene pasti}

Zapletene pasti delujejo kot običajne pasti, le da ob sprožitvi vsak krog izvedejo zaporedje dejanj. V tem pogledu je soočenje z zapleteno pastjo precej podobno spopadu.

Ko se zapletena past sproži, najprej meče kocko za odzivnost. V opisu pasti je naveden prištevek k odzivnosti. Na svoji potezi se past ponovno sproži in večinoma izvede dejanje. To lahko pomeni, da proti pustolovcem naredi nekaj zaporednih napadov, ustvari učinek, ki se skozi čas spreminja, ali predstavi kakšen drug dinamičen izziv. Razen tega zaznavanje ali zaobhajanje zapletene pasti deluje enako kot pri običajnih pasteh.

\section{Strupi}

Strupi so zaradi svoje zlohotne in smrtonosne narave prepovedani v mnogih družbah, vendar ostajajo priljubljeni med atentatorji, temlinci in drugimi hudobnimi bitji.

\subsubsection{Vrste strupov}

Obstajajo štiri vrste strupov.

\subparagraph{Dotikalen}
Dotikalen strup je mogoče nanesti na predmet, kjer ohranja učinkovitost, dokler se ga ne dotaknemo ali ga speremo s predmeta. Bitje, ki se dotakne dotikalnega strupa z golo kožo, utrpi njegove posledice.

\subparagraph{Zaužit}
Bitje utrpi posledice strupa, če pogoltne cel odmerek zaužitega strupa. Zaužije ga lahko s hrano ali pijačo. Če tako presodiš, lahko nepopoln odmerek povzroči milejše učinke (bitje ima lahko na primer olajšavo na rešilni met proti strupu ali pa utrpi le polovico škode, če mu rešilni met ne uspe).

\subparagraph{Vdihnjen}
Ti strupi so prahovi ali plini, ki nas zastrupijo, če jih vdihnemo. Če pihnemo prah ali izpustimo plin, njegove posledice trpijo vsa bitja v območju 1,5-metrske kocke. Takoj potem se oblak strupa razblini. Pred vdihnjenim strupom se ni mogoče zaščititi z zadrževanjem sape, saj učinkuje na nosno sluznico, solzne žleze in druge dele telesa.

\subparagraph{Poškodben}
Poškodbeni strup nanašamo na orožja, strelivo, komponente pasti in druge predmete, ki prebadajo ali sekajo. Ta strup ostane učinkovit, dokler ne pride v rano ali dokler ga ne speremo proč. Bitje, ki utrpi poškodbe zaradi sekanja ali prebadanja z zastrupljenim predmetom, trpi za njegovimi posledicami.

\subsection{Primeri strupov}

\begin{DndTable}{Xlr}
    \textbf{Strup} & \textbf{Vrsta} & \textbf{Cena na odmerek} \\
    Bleda tinktura & Zaužit & 250 zl \\
    Dim prežganega oturja & Vdihnjen & 500 zl \\
    Etrna esenca & Vdihnjen & 300 zl \\
    Gadjerepov strup & Poškodben & 1200 zl \\
    Kačji strup & Poškodben & 200 zl \\
    Laznikova sluz & Dotikalen & 200 zl \\
    Morilčeva kri & Zaužit & 150 zl \\
    Mrtvilo & Zaužit & 600 zl \\
    Polnočne solze & Zaužit & 1500 zl \\
    Serum resnice & Zaužit & 150 zl \\
    Strup vijoličnega črva & Poškodben & 2000 zl \\
    Taggitovo olje & Dotikalen & 400 zl \\
    Temlinski strup & Poškodben & 200 zl \\
    Žlehtnoba & Vdihnjen & 250 zl
\end{DndTable}

\subsubsection{Opisi strupov}

Vsak strup ima svojevrstne učinke.

\subparagraph{Bleda tinktura (zaužita)}
Bitje mora opraviti rešilni met Konstitucije s TR 16, drugače utrpi 3 (1k6) točke škode zaradi zastrupitve in postane zastrupljeno. Zastrupljeno bitje mora ponoviti rešilni met vsakih 24 ur in ob neuspehu utrpeti 3 (1k6) točk škode zaradi zastrupitve. Dokler strup ne preneha delovati, škode, ki jo povzroči strup, ni mogoče pozdraviti na noben način. Po sedmih uspešnih rešilnih metih se učinek konča in bitje se lahko normalno pozdravi.

\subparagraph{Dim prežganega oturja (vdihnjen)}
Bitje mora opraviti rešilni met Konstitucije s TR 13, drugače utrpi 10 (3k6) točk škode zaradi zastrupitve. Rešilni met mora ponavljati ob začetku vsake svoje poteze. Za vsak zaporeden neuspel rešilni met lik utrpi 3 (1k6) točke škode zaradi zastrupitve. Po treh uspešnih rešilnih metih strup preneha delovati.

\subparagraph{Temlinski strup (poškodben)}
Ta strup tipično izdelujejo le temni vilinci in le v krajih, kamor sončna svetloba ne seže. Bitje, ki se zastrupi z njim, mora uspešno opraviti rešilni met Konstitucije s TR 13, drugače je zastrupljeno za 1 uro. Če rešilni met ne uspe in je razlika med TR-jem in izidom meta 5 ali več, bitje pade tudi v nezavest. Bitje se predrami, če utrpi poškodbe ali če ga drugo bitje dregne.

\subparagraph{Etrna esenca (vdihnjena)}
Bitje mora opraviti rešilni met Konstitucije s TR 15, drugače je zastrupljeno za 8 ur. Zastrupljeno bitje je tudi nezavestno. Bitje se predrami, če utrpi poškodbe ali če ga drugo bitje dregne.

\subparagraph{Gadjerepov strup (poškodben)}
Strup se pridobiva iz mrtvih ali onesposobljenih gadjerepov. Bitje mora opraviti rešilni met Konstitucije s TR 15. Če mu ne uspe, utrpi 24 (7k6) točk škode zaradi zastrupitve, drugače le pol toliko.

\subparagraph{Kačji strup (poškodben)}
Ta strup se pridobiva iz mrtvih ali onesposobljenih orjaških strupenih kač. Bitje, ki se z njim zastrupi, mora opraviti rešilni met Konstitucije s TR 11. Če mu ne uspe, utrpi 10 (3k6) točk škode zaradi zastrupitve, drugače pa le pol toliko.

\subparagraph{Laznikova sluz (dotikalna)}
Ta strup se pridobiva iz mrtvih ali onesposobljenih laznikov. Bitje mora opraviti rešilni met Konstitucije s TR 13, drugače je zastrupljeno za 1 minuto. Zastrupljeno bitje je paralizirano. Bitje lahko ponovi rešilni met ob koncu vsake svoje poteze. Čim mu rešilni met uspe, se učinek strupa zbledi.

\subparagraph{Morilčeva kri (zaužita)}
Bitje mora opraviti rešilni met Konstitucije s TR 10. Če mu ne uspe, utrpi 6 (1k12) točk škode zaradi zastrupitve in je zastrupljeno za 24 ur. Če mu uspe, utrpi le pol toliko škode in ni zastrupljeno.

\subparagraph{Mrtvilo (zaužito)}
Bitje mora opraviti rešilni met Konstitucije s TR 15, drugače postane zastrupljeno za 4k6 ur. Zastrupljeno bitje je onesposobljeno.

\subparagraph{Polnočne solze (zaužite)}
Bitje, ki zaužije strup, ne občuti učinka, dokler ne odbije polnoč. Če strup do takrat ni nevtraliziran, mora bitje opraviti rešilni met Konstitucije s TR 17. Ob neuspehu utrpi 31 (9k6) točk škode zaradi zastrupitve, drugače le pol toliko.

\subparagraph{Serum resnice (zaužit)}
Bitje mora opraviti rešilni met Konstitucije s TR 11, drugače postane zastrupljeno za 1 uro. Zastrupljeno bitje ne more vede govoriti laži, kot če bi bilo pod urokom \textit{območje resnice}.

\subparagraph{Strup vijoličnega črva (poškodben)}
Strup se pridobiva iz mrtvih ali onesposobljenih vijoličnih črvov. Bitje mora opraviti rešilni met Konstitucije s TR 19. Če mu ne uspe, utrpi 42 (12k6) točk škode zaradi zastrupitve, drugače pa le pol toliko.

\subparagraph{Taggitovo olje (dotikalno)}
Bitje mora opraviti rešilni met Konstitucije s TR 13, drugače postane zastrupljeno za 24 ur. Zastrupljeno bitje je nezavestno. Bitje se predrami, če ga kaj poškoduje.

\subparagraph{Žlehtnoba (vdihnjena)}
Bitju mora uspeti rešilni met Konstitucije s TR 15, drugače postane zastrupljeno za 1 uro. Zastrupljeno bitje je zaslepljeno.

\section{Bolezni}

Po kraljestvu razsaja kuga, zato se pustolovci podajo iskat zdravilo. Junakinja stopi na plan iz stare, stoletja zapečatene grobnice in kmalu začne trpeti za neznano boleznijo. Coprnik užali temačno silo in se naleze čudne betege, ki se razširi, kadarkoli pričara nov urok.

Preprosto obolenje lahko porabi le prgišče pustolovskih sredstev in ga je mogoče ustaviti z urokom \textit{manjše obnovitve}. Hujši izbruhi bolezni so lahko povod za eno ali več pustolovščin, kjer liki iščejo zdravilo, ustavljajo širjenje bolezni in se ukvarjajo s posledicami epidemije.

Bolezen, ki ima večji vpliv kot samo kratkoročno obolevanje nekaj članov skupine, je pomembna za zgodbo. Pravila pomagajo opisati posledice bolezni in zdravilo zanjo, a podrobnosti delovanja bolezni so lahko poljubne. Z boleznijo se lahko okuži katerokoli bitje in za posamezne ujme se lahko odločiš, ali prehajajo z rase na raso ali ne. Gripa lahko prizadene le konstrukte ali nemrtve ali pa zdesetka polovnjaško sosesko, dočim ostale rase ostanejo neprizadete. Važna je zgodba, ki jo želiš povedati.

Primeri bolezni so recimo krohotalna bolezen, greznična kuga in gledognitje. Bolezni imajo lahko različne TR-je za rešilne mete pred okužbo, inkubacijske dobe, simptome in druge karakteristike, ki jih lahko prilagodiš svojim potrebam.

\section{Blaznost}

V značilni kampanji liki ne znorijo zaradi grozot in razdejanja, ki so jima priča dan za dnem, toda včasih je lahko pritisk pustolovstva preprosto premočan. Če sta tematika tvoje kampanje v veliki meri groza in strah, lahko igro še dodatno začiniš z blaznostjo in tako poudariš grozljivost nevarnosti, s katerimi se borijo pustolovci.

\subsection{Kako zblazneti}

Zdrav um lahko zmračijo različni magični učinki. Nekateri uroki, na primer \textit{klic na drugo planoto} ali \textit{simbol}, lahko povzročijo blaznost, zato lahko namesto pravil teh urokov uporabiš kar tukajšnja pravila za blaznost. V norost te lahko pahnejo tudi bolezni, strupi in planotni učinki, kot sta duševna burja ali predirljivi vetrovi Pandemonija. Tudi predmeti, ki jih liki uporabljajo ali se z njimi uglasijo, lahko načnejo duševno zdravje.

Upiranje blaznosti običajno zahteva rešilni met Modrosti ali Karizme.

\subsection{Posledice blaznosti}

Blaznost je lahko kratkotrajna, dolgotrajna ali trajna. Večina običajnih učinkov povzroči kratkotrajno blaznost, ki traja le nekaj minut. Bolj grozljivi učinki ali več združenih učinkov privede do dolgotrajne ali trajne blaznosti.

Lik s \textbf{kratkotrajno blaznostjo} trpi za učinkom iz preglednice Kratkotrajna blaznost za 1k10 minut.

Lik z \textbf{dolgotrajno blaznostjo} trpi za učinkom iz preglednice Dolgotrajna blaznost za 1k10 x 10 ur.

Lik s \textbf{trajno blaznostjo} trpi za učinkom iz preglednice Trajna blaznost, ki traja, dokler se ga ne pozdravi.

\begin{DndTable}[header=Kratkotrajna blaznost]{lX}
    \textbf{k100} & \textbf{Učinek (traja 1k10 minut)} \\
    01 - 20 & Lik se zapre v svoje misli in postane paraliziran. Učinek se konča, če lik utrpi kakršnokoli škodo. \\
    21 - 30 & Lik postane onesposobljen in v tem času kriči, se smeji ali ihti. \\
    31 - 40 & Lik postane prestrašen in mora na vsaki svoji potezi porabiti svoja dejanja in gibanje za to, da beži pred izvorom svojega strahu. \\
    41 - 50 & Lik začne nepovezano blebetati in ni sposoben normalnega govora in čaranja. \\
    51 - 60 & Lik mora na vsaki potezi porabiti svoje dejanje za to, da napade najbližje bitje. \\
    61 - 70 & Lik doživlja slikovite privide in ima otežitev na preizkuse sposobnosti. \\
    71 - 75 & Lik stori karkoli, kar mu kdorkoli naroči, razen če je očitno samouničujoče. \\
    76 - 80 & Lik doživlja močno željo po tem, da bi jedel nekaj čudnega in ogabnega, na primer zemljo, sluz ali drobovino. \\
    81 - 90 & Lik je pretresen. \\
    91 - 100 & Lik pade v nezavest.
\end{DndTable}

\begin{DndTable}[header=Dolgotrajna blaznost]{lX}
    \textbf{k100} & \textbf{Učinek (traja 1k10 x 10 ur)} \\
    01 - 10 & Lik si ne more pomagati, da ne bi počel iste stvari znova in znova, na primer si umival roke, se dotikal predmetov, molil ali štel kovancev. \\
    11 - 20 & Lik doživlja slikovite privide in ima otežitev na preizkuse sposobnosti. \\
    21 - 30 & Lik ima hudo preganjavico. Ima otežitev na preizkuse Modrosti in Karizme. \\
    31 - 40 & Lik čuti hud odpor do nečesa (običajno izvora blaznosti), kot da bi nanj vplival učinek sovražnosti uroka \textit{sovražnost/naklonjenost}. \\
    41 - 45 & Liku se začne hudo blesti. Izberi napoj. Lik si domišlja, da je pod učinkom tega uroka. \\
    46 - 55 & Lik se naveže na \textquote{srečno osebo/predmet} in ima otežitev na napadalne mete, preizkuse sposobnosti in rešilne mete, ko je od njega/nje oddaljen dlje kot 9 metrov. \\
    56 - 65 & Lik je zaslepljen (25 \%) ali oglušel (75 \%). \\
    66 - 75 & Lik popadejo nenadzorovani trzljaji ali tiki, ki povzročajo otežitev na napadalne mete, preizkuse sposobnosti in rešilne mete, ki se tičejo Moči ali Gibčnosti. \\
    76 - 85 & Lik trpi za delno izgubo spomina. Ve, kdo je, in ohranja svoje rasne lastnosti in razredne značilnosti, vendar ne prepozna drugih ljudi in se ne spomni dogodkov iz časa, preden je zblaznel. \\
    86 - 90 & Kadar lik utrpi škodo, mora uspešno opraviti rešilni met Modrosti s TR 15, ali pa trpi enake posledice, kot če bi mu spodletel rešilni met proti uroku \textit{zmede}. Posledice trajajo 1 minuto. \\
\end{DndTable}

\newpage

\begin{DndTable}{lX}
    91 - 95 & Lik izgubi sposobnost govora. \\
    96 - 100 & Lik pade v nezavest. Dreganje in poškodbe ga ne morejo prebuditi.
\end{DndTable}

\begin{DndTable}[header=Trajna blaznost]{lX}
    \textbf{k100} & \textbf{Hiba (traja, dokler se ne pozdravi)} \\
    01 - 15 & \textquote{Zapitost me drži pri pameti.} \\
    16 - 25 & \textquote{Obdržim, karkoli najdem.} \\
    26 - 30 & \textquote{Poskušam biti bolj podoben/a nekomu, ki ga poznam. Oblačim se kot on/a, se obnašam kot on/a in celo prevzamem njegovo/njeno ime.} \\
    31 - 35 & \textquote{Drugim ljudem sem zanimiv samo, če priredim resnico, pretiravam ali preprosto lažem.} \\
    36 - 45 & \textquote{Izpolnitev mojega cilja je edino, kar me zanima. Vse ostalo bom preprosto ignoriral/a.} \\
    46 - 50 & \textquote{Le s težavo se posvetim vsemu, kar se dogaja okoli mene.} \\
    51 - 55 & \textquote{Ni mi všeč, kako me ljudje ves čas obsojajo.} \\
    56 - 70 & \textquote{Sem najpametnejša, najmodrejša, najhitrejša in najlepša oseba, kar jih poznam.} \\
    71 - 80 & \textquote{Prepričan/a sem, da me lovijo mogočni sovražniki. Njihovi pristaši so povsod, kamor grem. Prepričan/a sem, da me ves čas opazujejo.} \\
    81 - 85 & \textquote{Zaupam lahko le eni osebi. Ampak samo jaz lahko vidim tega/o posebnega/o prijatelja/ico.} \\
    86 - 95 & \textquote{Ničesar ne morem jemati resno. Resnejša je situacija, bolj mi je smešna.} \\
    96 - 100 & \textquote{Odkril/a sem, da mi je pobijanje ljudi zelo, zelo všeč.}
\end{DndTable}

\subsection{Zdravljenje blaznosti}

Urok \textit{pomiritve čustev} lahko zaduši posledice blaznosti, urok \textit{manjše obnovitve} pa lahko lik reši kratko- ali dolgotrajne blaznosti. Pri določenih izvorih blaznosti se lahko za učinkovita izkažeta tudi uroka \textit{izničenje prekletstva} in \textit{odbijanje dobrega in zlega}. Lik se lahko reši trajne norosti le z urokom \textit{obsežnejše obnovitve} ali z močnejšo magijo.