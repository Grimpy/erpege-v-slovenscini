\chapter{Uporaba ocen sposobnosti}

Telesne in duševne karakteristike vsakega bitja opisuje šest ocen sposobnosti:

\begin{itemize}
    \item[] \textbf{Moč} predstavlja telesno moč,
    \item[] \textbf{Gibčnost} predstavlja okretnost,
    \item[] \textbf{Konstitucija} predstavlja vzdržljivost,
    \item[] \textbf{Inteligenca} predstavlja zmožnost sklepanja in spomin,
    \item[] \textbf{Modrost} predstavlja dojemanje ter razumevanje in
    \item[] \textbf{Karizma} predstavlja izrazitost osebnosti.
\end{itemize}

Je lik mišičast in pronicljiv? Genialen in prikupen? Okreten in žilav? Vse to določajo ocene sposobnosti.

Na ocene sposobnosti se opirajo trije glavni meti kock v igri -- preizkus sposobnosti, rešilni met in napadalni met. Že v uvodu smo opisali temeljni potek teh metov: vrzi k20, prištej popravek, izpeljan iz ene izmed šestih ocen sposobnosti, in končni seštevek primerjaj s ciljnim številom.

V tem poglavju obdelamo pomen preizkusov sposobnosti in rešilnih metov. Pravila za napadalne mete so zapisana v devetem poglavju.

\section{Ocene sposobnosti in popravki}

Vsaka sposobnost ima svojo oceno, ki določa jakost te sposobnosti. Ocena sposobnosti ni le prikaz prirojenih zmožnosti, temveč zajema tudi izurjenost in znanje v povezavi s to sposobnostjo.

Ocena 10 ali 11 je normalno povprečje za človeka, toda večina sposobnosti pustolovcev in številnih pošasti je nadpovprečnih. Pogosto je najvišja ocena, ki jo posameznik doseže, 18. Pustolovci lahko prilezejo vse do ocene 20, pošasti in božanska bitja pa celo do 30.

Vsaka sposobnost ima svoj popravek, čigar vrednosti segajo vse od -5 (za oceno 1) do +10 (za oceno 30). Preglednica ocen sposobnosti in popravkov prikazuje preslikave iz ocen v popravke.

Popravek lahko izračunaš tudi brez tabele. Od ocene sposobnosti odštej 10 in razliko deli z 2 (zaokroženo navzdol).

Ker popravke sposobnosti prištevamo skoraj vsakemu napadalnemu metu, preizkusu sposobnosti in rešilnemu metu, jih uporabljamo veliko pogosteje kot ocene sposobnosti.

\begin{DndTable}[header=Ocene sposobnosti in popravki,width=\linewidth]{XX}
    \textbf{Ocena} & \textbf{Popravek} \\
    1 & -- 5 \\
    2-3 & -- 4 \\
    4-5 & -- 3 \\
    6-7 & -- 2 \\
    8-9 & -- 1 \\
    10-11 & +0 \\
    12-13 & +1 \\
    14-15 & +2 \\
    16-17 & +3 \\
    18-19 & +4 \\
    20-21 & +5 \\
    22-23 & +6 \\
    24-25 & +7 \\
    26-27 & +8 \\
    28-29 & +9 \\
    30 & +10
\end{DndTable}

\section{Olajšava in otežitev}

Včasih posebna sposobnost ali urok na tvoj preizkus sposobnosti, rešilni met ali napadalni met uvaja olajšavo ali otežitev. V tem primeru meči dve kocki k20. Če imaš olajšavo, uporabi višjega izmed obeh izidov, če pa imaš otežitev, izberi nižjega. Primer: če imaš otežitev in mečeš 17 ter 5, uporabiš 5. Če imaš olajšavo in vržeš te isti cifri, uporabiš 17.

Če se pojavi več okoliščin, ki uveljavljajo olajšavo oziroma otežitev, ne mečeš več kot dve kocki. Na primer, če dve ugodni okoliščini določata, da imaš olajšavo, še vedno mečeš le dve k20.

Če so okoliščine takšne, da ene podeljujejo olajšavo, druge pa otežitev, se učinka izničita in k20 mečeš le enkrat. To velja tudi takrat, ko več okoliščin podeljuje otežitev in le ena olajšavo, in obratno.

Če imaš olajšavo ali otežitev in ti nek del igre, denimo polovnjakova lastnost S srečo v žepu, dovoli, da k20 ponovno mečeš ali zamenjaš, lahko ponovno vržeš ali zamenjaš le eno izmed kock. Katero, izbereš sam/a. Na primer, če ima polovnjak olajšavo ali otežitev na preizkus sposobnosti in vrže 1 in 13, lahko uporabi svojo lastnost S srečo v žepu, da ponovno vrže kocko z izidom 1.

Olajšavo ali otežitev običajno prejmeš zaradi posebnih sposobnosti, dejanj ali urokov. Olajšavo lahko podeli tudi navdih (to je razloženo v četrtem poglavju, Osebnost in ozadje). GT lahko odloči, da posamezne okoliščine vplivajo na verjetnost izida meta, in si zato deležen/a bodisi olajšave bodisi otežitve.

\section{Izvedenski prištevek}

Kot je opisano že v prvem poglavju, ima vsak lik svoj izvedenski prištevek, ki je določen glede na likovo stopnjo. Tudi pošasti ga imajo; upoštevan je v njihovih okvirčkih lastnosti. Prištevek prištevamo k preizkusom sposobnosti, rešilnim metom in napadalnim metom.

Svojega izvedenskega prištevka ne moreš nanesti posameznemu metu kocke ali drugemu številu več kot enkrat. Če recimo dve različni pravili narekujeta, da moraš svoj izvedenski prištevek prišteti rešilnemu metu Modrosti, prištevek prišteješ le enkrat.

Občasno se zgodi, da moraš svoj izvedenski prištevek pomnožiti ali deliti (na primer podvojiti ali prepoloviti), preden ga uporabiš. Taka je tudi rokomavhova značilnost Izvedenstvo, ki ob nekaterih preizkusih sposobnosti izvedenski prištevek podvoji. Če okoliščine diktirajo, da bi bilo smiselno izvedenski prištevek prišteti večkrat istemu metu, ga kljub temu prišteješ samo enkrat in pomnožiš oziroma deliš samo enkrat.

Če opravljaš preizkus sposobnosti, ki ti ne omogoča uporabe izvedenskega prištevka, obenem pa ti neka značilnost ali učinek narekuje, da moraš prištevek pred uporabo zmnožiti, izvedenskega prištevka vseeno ne prišteješ preizkusu. To utemeljimo tako: izvedenski prištevek za preizkus sposobnosti, kjer prištevka ne moreš koristiti, je 0, in če katerokoli drugo število pomnožimo z 0, še zmeraj dobimo 0. Na primer, če nisi izveden/a v Zgodovini, ti značilnost, po kateri lahko svojim preizkusom Inteligence (Zgodovina) prišteješ dvakratnik svojega izvedenskega prištevka, čisto nič ne koristi.

V splošnem pri rešilnih in napadalnih metih ne množiš svojega izvedenskega prištevka. Če ti značilnost ali učinek to dopušča, veljajo zgornja pravila.

\section{Preizkusi sposobnosti}

Preizkus sposobnosti preverja prirojeno nadarjenost in izurjenost lika ali pošasti pri premagovanju izzivov. GT zahteva preizkus sposobnosti, ko bitje začne izvajati dejanje (vse drugo razen napada), za katerega obstaja možnost neuspeha. Ko izid ni nedvomen, ga določijo kocke.

Pri vsakem preizkusu sposobnosti GT določi sposobnost, ki najbolj vpliva na izvedbo dejanja v vprašanju, in težavnost naloge v obliki težavnostnega razreda. Zahtevnejša je naloga, višji je TR. Preglednica tipičnih težavnostnih razredov prikazuje najpogostejše TR-je.

Preizkus sposobnosti izvedeš tako, da vržeš k20 in prišteješ ustrezen popravek sposobnosti. Kot velja že za druge mete k20, prištej ustrezne prištevke in odbitke, na koncu pa rezultat primerjaj s TR-jem. Če je rezultat enak ali večji od TR, je preizkus sposobnosti uspel in bitje prestane izziv. V nasprotnem primeru preizkus ni uspel in bitje ne opravi izziva oziroma ga le delno opravi, če tako določi GT.

\begin{DndTable}[header=Tipični težavnostni razredi]{XX}
    \textbf{Zahtevnost naloge} & \textbf{TR} \\
    Zelo lahka & 5 \\
    Lahka & 10 \\
    Povprečna & 15 \\
    Težka & 20 \\
    Zelo težka & 25 \\
    Skoraj neizvedljiva & 30
\end{DndTable}

\subsection{Tekma}

Včasih si lik ali pošast prizadeva za nekaj, kar je v direktnem nasprotju s prizadevanji drugega lika ali pošasti. To se lahko zgodi, ko obe bitji poskušata doseči isto reč in lahko uspe le enemu izmed njiju. Primer: obe sežeta po čarobnem prstanu, ki je padel po tleh. Do iste situacije pride, če eno bitje poskuša drugemu preprečiti doseganje nekega cilja -- na primer, ko pošast poskuša vdreti skozi vrata, ki jih na drugi strani zadržuje pustolovec. V takih primerih izid določa preizkus sposobnosti posebne sorte, imenovan tekma.

Oba udeleženca tekme izvedeta za svoje dejanje ustrezen preizkus sposobnosti. Prištejeta vse potrebne prištevke in odbitke. Toda končnih rezultatov ne primerjata s TR-jem, temveč jih primerjata med seboj. Udeleženec z višjim preizkusom zmaga. Lik oziroma pošast bodisi uspešno izvede dejanje ali prepreči uspeh drugemu bitju.

Če je izid neodločen (preizkusa sta enaka), je položaj enak kot pred začetkom tekme. Tako lahko eden izmed udeležencev privzeto zmaga. Vrnimo se na prejšnji primer -- če sta tekmovalca, ki sežeta po prstanu na tleh, izenačena, nobeden izmed njiju ne dobi prstana. Pri tekmi, kjer pošast poskuša odpreti vrata, ki jih zadržuje pustolovec, neodločen izid pomeni, da vrata ostanejo zaprta (privzeto zmaga pustolovec).

\subsection{Veščine}

Vsaka sposobnost obsega neko količino veščin. Veščina predstavlja specifičen vidik ocene sposobnosti, posameznikovo izvedenstvo v neki veščini pa nakazuje njegovo usmerjenost v ta vidik. (Likovo začetno izvedenstvo v veščinah določimo ob stvaritvi lika, izvedenstvo pošasti pa najdemo v okvirčku lastnosti).

Podajmo primer. S preizkusom Gibčnosti lahko napovemo, ali liku uspe izvesti akrobacijo, nekam skrivaj podtakniti predmet ali se skriti. Vsak izmed teh vidikov Gibčnosti se preslika v ustrezno veščino: Akrobatiko, Rokohitrstvo in Prikritost. Lik, ki je izveden v Prikritosti, je še zlasti uspešen pri preizkusih Gibčnosti v povezavi s tihotapljenjem in skrivanjem.

Spodnji seznam prikazuje veščine in sposobnosti, h katerim spadajo. (Konstitucija ne botruje nobeni veščini.) V kasnejših razdelkih tega poglavje so opisi posameznih sposobnosti, kjer je tudi nekaj primerov na temo, kako uporabiti veščino v povezavi s sposobnostjo.

Včasih bo GT prosil, da izvedeš preizkus določene veščine -- na primer: \textquote{Izvedi preizkus Modrosti (Zaznavanje).} Včasih lahko igralec vpraša GT-ja, ali se lahko izvedenstvo v določenih veščinah upošteva pri preizkusu. V vsakem primeru izvedenstvo v veščini pomeni, da lahko posameznik prišteje svoj izvedenski prištevek preizkusom sposobnosti, ki vključujejo to veščino. Če v veščini ni izveden, opravi le navaden preizkus sposobnosti.

Primer: če lik poskusi splezati po nevarni pečini, lahko Gospodar temnic prosi za preizkus Moči (Atletika). Če je lik izveden v Atletiki, preizkusu Moči prištejemo likov izvedenski prištevek. Če lik ni izveden v Atletiki, opravi le navaden preizkus Moči.

\begin{DndTable}{XX}
    {
        \textbf{Moč}

        \hspace{10pt}Atletika

        \textbf{Gibčnost}

        \hspace{10pt}Akrobatika
        
        \hspace{10pt}Prikritost
        
        \hspace{10pt}Rokohitrstvo

        \textbf{Inteligenca}

        \hspace{10pt}Čudoslovje
        
        \hspace{10pt}Naravoslovje
        
        \hspace{10pt}Preiskovanje

        \hspace{10pt}Veroslovje

        \hspace{10pt}Zgodovina
    } & {
        \textbf{Modrost}

        \hspace{10pt}Čut za živali

        \hspace{10pt}Preživetje

        \hspace{10pt}Uvid

        \hspace{10pt}Zaznavanje

        \hspace{10pt}Zdravilstvo

        \textbf{Karizma}

        \hspace{10pt}Napeljevanje

        \hspace{10pt}Nastopanje

        \hspace{10pt}Ustrahovanje

        \hspace{10pt}Zavajanje
    }
\end{DndTable}

\subsubsection{Varianta: Veščine z različnimi sposobnostmi}

Običajno svoje izvedenstvo v veščini upoštevaš le pri specifičnem preizkusu sposobnosti. Izvedenstvo v Atletiki denimo ponavadi šteje le pri preizkusih Moči. V nekaterih primerih, ko je to smiselno, pa svoje izvedenstvo lahko naneseš na drugačne vrste preizkusov. V takih primerih lahko GT prosi za preizkus neobičajne kombinacije sposobnosti in veščine ali pa ti prosiš GT-ja, da ti kaj takega dovoli. Podajmo primer: če moraš priplavati z otoka do celine, ti lahko GT odredi preizkus Konstitucije, da preveri, ali si dovolj vzdržljiv/a, da ti podvig uspe. A ob tem ti lahko dovoli, da upoštevaš svoje izvedenstvo v Atletiki, in zaprosi za preizkus Konstitucije (Atletika). Drug primer bi bil, da se odločiš, da tvoj škratji borec s surovo silo ustrahuje sovražnika. GT te lahko prosi, da opraviš preizkus Moči (Ustrahovanje), čeprav Ustrahovanje navadno povezujemo s Karizmo.

\subsection{Pasivni preizkusi}

Pasivni preizkus je poseben tip preizkusa sposobnosti, ki ne vpleta metanja kock. Takšen preizkus je lahko povprečje izidov večkratnega izvajanja nekega opravila, kot je na primer neprestano iskanje skrivnih vrat, ali pa ga GT uporabi takrat, ko na skrivaj, brez metanja kock, želi določiti, ali liku nekaj uspe -- na primer, ali lik opazi skrito pošast.
Spodaj je formula za likov pasivni preizkus:

\begin{center}
    10 + vsi popravki,\\ki jih običajno prištejemo preizkusu
\end{center}

Če ima lik olajšavo na preizkus, zgornji vsoti prištejemo še 5. Če ima otežitev, odštejemo 5. V igri končnemu izidu pasivnega preizkusa rečemo \textbf{ocena}.

Če ima na primer prvostopenjski lik oceno Modrosti 15 in je izveden v Zaznavanju, ima oceno pasivne Modrosti (Zaznavanje) 14.

Na pasivne preizkuse se opirajo pravila skrivanja v razdelku \textquote{Gibčnost} nekoliko nižje in pravila raziskovanja v osmem poglavju.

\subsection{Delo v skupini}

Včasih se nekega dela polotita dva ali več likov. Lik, ki vodi izvedbo opravila -- ali tisti, ki ima najvišji popravek sposobnosti --, lahko napravi preizkus sposobnosti z olajšavo, ki nakazuje pomoč ostalih likov. V spopadu je za to potrebno dejanje Pomoči (glej deveto poglavje).

Lik lahko pomaga le, če je naloga takšna, da bi jo lahko izvedel tudi sam. Na primer, če nek lik poskuša vlomiti ključavnico, kar zahteva izvedenstvo s tatinskim orodjem, drugi lik, ki s tem orodjem ni izveden, prvemu ne more pomagati. Poleg tega lahko lik pomaga samo, če je dejansko korist od tega, da se dela polotita dva ali več posameznikov. Nekatera opravila, denimo vdevanje sukanca v šivanko, niso nič lažja s tujo pomočjo.

\subsubsection{Skupinski preizkusi}

Ko posamezni liki poskušajo nekaj storiti kot skupina, lahko GT zaprosi za skupinski preizkus sposobnosti. V takih primerih liki, ki so vešči določenega opravila, krijejo tiste, ki niso.

Skupinski preizkus sposobnosti se izvede tako, da vsi v skupini vršijo preizkus sposobnosti. Če vsaj polovica skupine uspešno opravi preizkus, uspe vsa skupina. V nasprotnem primeru je skupina neuspešna.

Skupinski preizkusi niso pogosti in so najuporabnejši, ko so vsi liki uspešni ali neuspešni kot skupina. Primer bi bil, da pustolovci brodijo skozi močvaro. GT zahteva skupinski preizkus Modrosti (Preživetje), s katerim se bo pokazalo, ali se liki izognejo živemu pesku, požiralnikom in drugim naravnim nevarnostim močvirja. Če uspe vsaj polovici skupine, lahko uspešni liki pomagajo svojim tovarišem iz nevarnosti. Drugače se skupina znajde v težavah.

\section{Uporaba sposobnosti}

Vsako opravilo, ki ga izvaja lik ali pošast, je mogoče predstaviti z eno izmed šestih sposobnosti. V tem razdelku je podrobneje razloženo, kaj sposobnosti pomenijo in na kakšne načine jih lahko uporabimo v igri.

\subsection{Moč}

Moč predstavlja telesno silo, atletsko izurjenost in jakost tvoje surove fizične moči.

\subsubsection{Preizkusi Moči}

Preizkus Moči lahko predstavlja kakršenkoli poskus dvigovanja, potiskanja, vleke ali lomljenja nečesa, prerivanja svojega telesa skozi kaj ali splošno uporabo surove sile. Veščina Atletike pomeni nadarjenost v nekaterih primerih uporabe Moči.

\subparagraph{Atletika}
Preizkus Moči (Atletika) uporabiš, kadar naletiš na težave med plezanjem, skakanjem ali plavanjem. Spodaj je navedenih nekaj primerov takih dejavnosti:

\begin{itemize}
    \item Poskušaš prečiti zlizano ali spolzko pečino, se izogibati padajočemu kamenju med plezanjem po zidu ali se oprijemati nečesa, kar se te hoče otresti.
    \item Poskušaš preskočiti precej širok jarek ali med skokom izvesti akrobacijo.
    \item Trudiš se plavati ali ohranjati glavo nad gladino sredi deroče reke ali v vodi z gosto morsko travo. Ali pa te neko bitje poskuša potegniti pod vodo ali ti drugače otežiti plavanje.
\end{itemize}

\subparagraph{Ostali preizkusi Moči}
GT lahko zaprosi za preizkus Moči, ko opravljaš spodnja opravila:

\begin{itemize}
    \item Vdreš v zagozdena, zapahnjena ali zadelana vrata.
    \item Rešiš se iz vezi ali okov.
    \item Prerineš se skozi pretesen rov.
    \item Držiš se za drveč voz, ki te vleče za seboj.
    \item Podreš kip.
    \item Držiš skalo, da se ne skotali proč.
\end{itemize}

\subsubsection{Napadalni meti in škoda}

Kadar napadaš z bližinskim orožjem, kot so gorjača, bojna sekira ali ostnik, napadalnemu metu in metu za določanje škode prišteješ svoj popravek Moči. Z bližinskim orožjem izvedeš bližnji napad v boju mož na moža, nekatera bližinska orožja pa lahko tudi vržeš in tako izvedeš oddaljeni napad.

\subsubsection{Dvigovanje in nošenje}

Tvoja ocena Moči določa količino bremena, ki ga lahko nosiš. Spodnji izrazi opisujejo, kaj lahko dvigneš ali nosiš.

\subparagraph{Nosilnost}
Tvoja nosilnost je tvoja ocena Moči krat 7. To je teža (v kilogramih), pod katero vzdržiš, in je dovolj visoka, da se večini likov običajno ni treba obremenjevati z njo.

\subparagraph{Potiskanje, vleka ali dvigovanje}
Lahko potiskaš, vlečeš ali dviguješ stvari, ki tehtajo največ dvakratnik tvoje nosilnosti v kilogramih (ali 14-kratnik tvoje ocene Moči). Če potiskaš ali vlečeš breme, ki presega tvojo nosilnost, tvoja hitrost pade na 1,5 metra.

\subparagraph{Velikost in moč}
Večja bitja lahko prenesejo večjo težo, drobna bitja pa manjšo. Za vsak velikostni razred nad povprečnim se njena nosilnost podvoji. Za drobno bitje nosilnost prepoloviš.

\subsubsection{Varianta: Obteženost}

Pravila dvigovanja in nošenja so namenoma preprosta. Če pa želiš podrobneje določiti likovo oviranost med nošenjem svoje opreme, navajamo še dodatno neobvezno pravilo. Če se odločiš zanj, zanemari stolpec \textquote{Moč} v preglednici oklepov v petem poglavju.

Če nosiš breme, ki je vsaj petkrat težje od tvoje ocene Moči, si \textbf{obtežen/a}, kar pomeni, da se tvoja hitrost zniža za 3 metre.

Če nosiš breme, ki je vsaj desetkrat težje od tvoje ocene Moči, a manjše od tvoje nosilnosti, si \textbf{huje obtežen/a}, kar pomeni, da se tvoja hitrost zniža za 6 metrov in imaš otežitev na preizkuse sposobnosti, napadalne mete in rešilne mete, ki uporabljajo Moč, Gibčnost ali Konstitucijo.

\subsection{Gibčnost}

Gibčnost meri okretnost, reflekse in ravnotežje.

\subsubsection{Preizkusi Gibčnosti}

Preizkus Gibčnosti lahko predstavlja kakršenkoli poskus urnega in pretanjenega gibanja, tiholazenja ali ohranjanje ravnotežja na nevarnem odseku. Akrobatika, Rokohitrstvo in Prikritost odražajo posamezne vrste preizkusov Gibčnosti.

\subparagraph{Akrobatika}
Preizkus Gibčnosti (Akrobatika) izvedeš takrat, ko se poskušaš obdržati na nogah v kočljivi situaciji, na primer ko poskušaš steči po ledeni podlagi, loviti ravnotežje na vrvi nad breznom ali stati na palubi zibajoče se ladje. GT lahko za preizkus Gibčnosti (Akrobatika) zaprosi tudi, kadar poskušaš izvesti akrobacije, kot so skok na glavo, preval, salta in premet.

\subparagraph{Prikritost}
Preizkus Gibčnosti (Prikritost) izvedeš, kadar se skriješ pred sovražniki, se pretihotapiš mimo stražarjev, neopažen/a smukneš proč ali se komu skrivaj prikradeš za hrbet.

\subparagraph{Rokohitrstvo}
Kadar poskusiš izvesti sleparijo ali prevaro, denimo nekomu nekaj podtakniti ali pri sebi skriti kak predmet, opraviš preizkus Gibčnosti (Rokohitrstvo). GT lahko za ta preizkus zaprosi tudi, kadar poskušaš komu kaj izmakniti.

\subparagraph{Drugi preizkusi Gibčnosti}
GT lahko zaprosi za preizkus Gibčnosti, ko poskušaš izvesti sledeča opravila:

\begin{itemize}
    \item Usmerjaš pošteno natovorjen voziček, ki se spušča po strmi klančini.
    \item Vodiš hitro kočijo okoli ostrega ovinka.
    \item Vlomiš ključavnico.
    \item Onesposobiš past.
    \item Trdno zvežeš ujetnika.
    \item Izviješ se iz vezi.
    \item Brenkaš na inštrument.
    \item Izdelaš majcen ali umetelen predmet.
\end{itemize}

\subsubsection{Napadalni meti in škoda}

Kadar napadaš z daljnosežnim orožjem, kot sta frača ali dolgi lok, napadalnemu metu in metu za določanje škode prišteješ svoj popravek Gibčnosti. Lahko ga prišteješ tudi takrat, ko napadaš z bližinskim orožjem, ki je odlikovano, kot sta denimo bodalo in rapir.

\subsubsection{Zaščitni razred}

Glede na to, kakšen oklep nosiš, lahko svojemu zaščitnemu razredu prišteješ delež ali celoto svojega popravka Gibčnosti. Več o tem piše v petem poglavju.

\subsubsection{Odzivnost}

Ob pričetku vsakega spopada določiš odzivnost tako, da opraviš preizkus Gibčnosti. Odzivnost določa, kako naj se bitja razporedijo v krogu spopada. Več o tem piše v devetem poglavju.

\subsection{Konstitucija}

Konstitucija meri zdravje, vzdržljivost in življenjsko silo.

\subsubsection{Preizkusi Konstitucije}

Preizkusi Konstitucije niso pogosti in zanje ni nobenih veščin, ker je moč lika, ki ga Konstitucija predstavlja, v veliki meri pasivna in ne zahteva posebnega truda s strani lika ali pošasti. Lahko pa preizkus Konstitucije predstavlja likov poskus, da prestopi normalne omejitve svojega telesa.

GT lahko zaprosi za preizkus Konstitucije, če poskusiš doseči kaj izmed naštetega:

\begin{itemize}
    \item Zadržuješ sapo.
    \item Hodiš ali delaš več ur brez počitka.
    \item Ponočuješ.
    \item Zdržiš brez hrane ali vode.
V enem požirku spiješ cel vrč piva.
\end{itemize}

\subsubsection{Življenjske točke}

Tvoj popravek Konstitucije se prišteva tvojim življenjskim točkam. Navadno popravek prišteješ izidu vsake življenjske kocke, ki jo vržeš pri določanju točk.

\begin{DndSidebar}{Skrivanje}
    GT določi, v kakšnih okoliščinah se je primerno skriti. Če se poskušaš skriti, izvedi preizkus Gibčnosti (Prikritost). Dokler te ne odkrijejo ali se sam/a ne nehaš skrivati, tvoj preizkus kontrira preizkusu Modrosti (Zaznavanje) kateregakoli bitja, ki te aktivno išče.

    Ne moreš se skriti pred bitjem, ki te jasno vidi. Če od sebe daš kakršenkoli zvok, na primer zakličeš tovarišem v opozorilo ali prevrneš vazo, izdaš svoje skrivališče. Nevidnega bitja ni mogoče videti, zato se vedno lahko poskusi skriti. Še vedno pa je mogoče videti njegove stopinje in ga slišati, če povzroča hrup.

    V spopadu večina bitij ves čas opreza za nevarnostmi z vseh strani. Če torej stopiš iz skrivališča in se približaš bitju, te bo najverjetneje opazilo. V nekaterih primerih lahko GT dovoli, da ostaneš skrit/a, ko se približuješ nepozornemu bitju, in tako prejmeš olajšavo na napad nanj, preden te opazi.

    \subparagraph{Pasivno zaznavanje}
    Ko si skrit/a, obstaja možnost, da te nekdo opazi, tudi če te ne išče aktivno. GT preveri, ali te bitje opazi, tako, da primerja tvoj preizkus Gibčnosti (Prikritost) z bitjevo oceno Modrosti (Zaznavanje), ki je enaka 10 + popravek Modrosti bitja + drugi prištevki oziroma odbitki. Če ima bitje olajšavo, prišteje še 5. Če ima otežitev, odšteje 5.

    Primer: če ima prvostopenjski lik (z izvedenskim prištevkom +2) oceno Modrosti 15 (popravek je potemtakem +2) in je izveden v Zaznavanju, ima pasivno Modrost (Zaznavanje) enako 14.

    \textbf{\textit{Kaj lahko vidiš?}} Eden glavnih dejavnikov, ki odločajo o tem, ali lahko najdeš skrito bitje ali predmet, je, kako dobro lahko vidiš na prizorišču, ki je lahko \textbf{rahlo} ali \textbf{pošteno nepregledno} -- o tem več v osmem poglavju.
\end{DndSidebar}

Če se tvoj popravek Konstitucije spremeni, se z njim zamenjajo tudi skupne življenjske točke, kakor da si nov popravek imel/a že od prve stopnje naprej. Na primer, če na četrti stopnji povišaš svoj popravek Konstitucije, da se poveča s +1 na +2, popraviš svoje skupne življenjske točke, kot da je bil popravek vedno +2. Tako prišteješ 3 življenjske točke za prve tri stopnje, potem pa mečeš kocko za življenjske točke na četrti stopnji in izidu prišteješ nov popravek. Nasproten primer bi bil, da ti na sedmi stopnji nek učinek zbije oceno Konstitucije in se ti popravek zmanjša za 1. Tedaj se tvoje skupne življenjske točke zmanjšajo za 7.

\subsection{Inteligenca}

Inteligenca zajema miselno ostrino, moč spomina in sposobnost logičnega razmišljanja.

\subsubsection{Preizkusi Inteligence}

Preizkus Inteligence je smiseln takrat, ko se moraš opreti na logiko, izobrazbo, spomin ali sklepanje. Veščine Čudoslovje, Naravoslovje, Preiskovanje, Veroslovje in Zgodovina odražajo izkušnje na nekaterih področjih Inteligence.

\subparagraph{Čudoslovje}
Preizkus Inteligence (Čudoslovje) meri, koliko v danem trenutku veš o urokih, čarovnih sredstvih, pošastnih znamenjih, čarovnih šegah, planotah obstoja in prebivalcih teh planot.

\subparagraph{Naravoslovje}
Preizkus Inteligence (Naravoslovje) meri, koliko se v danem trenutku spomniš o terenu, rastlinju in živalih, vremenu ter naravnih pojavih.

\subparagraph{Preiskovanje}
Kadar iščeš sledove in iz njih izpeljuješ ugotovitve, izvedeš preizkus Inteligence (Preiskovanje). Iz namigov lahko izluščiš nahajališče skritega predmeta, iz oblike rane ugotoviš, katero orožje jo je zadalo, ali določiš najbolj tvegano točko, ki bi lahko povzročila zrušenje predora. Tudi iskanje skritega zapisa v starih zvitkih lahko pomeni preizkus Inteligence (Preiskovanje).

\subparagraph{Veroslovje}
Preizkus Inteligence (Veroslovje) meri, koliko v danem trenutku veš o božanstvih, molitvah in obredih, verskih hierarhijah, svetih simbolih ter delovanju skrivnih kultov.

\subparagraph{Zgodovina}
Preizkus Inteligence (Zgodovina) meri, koliko v danem trenutku veš o zgodovinskih dogodkih, znamenitih osebnostih iz preteklosti, starih kraljestvih, davnih sporih, preteklih vojnah in pozabljenih civilizacijah.

\subparagraph{Ostali preizkusi Inteligence}
GT lahko zahteva preizkus Inteligence, ko se lotevaš česa podobnega kot:

\begin{itemize}
    \item Sporazumevaš se z bitjem brez uporabe besed.
    \item Oceniš vrednost dragocenega predmeta.
    \item Zamaskiraš se v mestnega stražarja.
    \item Ponarediš obrazec.
    \item Spomniš se podatkov o določeni obrti.
    \item Zmagaš v igri spretnosti.
\end{itemize}

\subsubsection{Uročna sposobnost}

Čarovniki za svojo uročno sposobnost uporabljajo Inteligenco. Uročna sposobnost jim pomaga določiti TR-je urokov, ki jih čarajo.

\subsection{Modrost}

Modrost odraža tvojo povezanost s svetom okoli tebe, tvoje zaznavanje in intuicijo.

\subsubsection{Preizkusi Modrosti}

Preizkus Modrosti lahko predstavlja poskus razločevanja telesne govorice, razumevanja sogovorčevih čustev, opažanja značilnosti okolice ali oskrbe poškodovanca. Modrost zajema veščine Čut za živali, Preživetje, Uvid, Zaznavanje in Zdravilstvo.

\subparagraph{Čut za živali}
Kadar je vprašanje, ali pomiriš udomačeno žival, brzdaš vpreženo žival ali sprevidiš namere živali, GT lahko odredi preizkus Modrosti (Čut za živali). Preizkus Modrosti (Čut za živali) opraviš tudi tedaj, ko poskušaš med nevarnim manevrom usmerjati žival, na kateri jahaš.

\begin{DndSidebar}{Odkrivanje skritih predmetov}
    Ko tvoj lik išče skrite stvari, kot so na primer skrivna vrata ali pasti, GT običajno zahteva od tebe preizkus Modrosti (Zaznavanje). S takim preizkusom lahko najdeš skrivne podrobnosti in druge informacije, ki bi jih drugače morda spregledal/a.

    V večini primerov moraš opisati, kje iščeš, da lahko GT določi verjetnost za tvoj uspeh. Na primer, ključ je skrit med perilom, ki je zloženo v vrhnjem predalu omare. Če rečeš GT-ju, da se razgleduješ po sobi in preiskuješ stene ter pohištvo, nimaš nobenih možnosti, da najdeš ključ, ne glede na rezultat tvojega preizkusa Modrosti (Zaznavanje). Če bi rekel/a, da odpiraš predale ali preiskuješ omaro, bi bile tvoje možnosti za uspešno odkritje ključa realnejše.
\end{DndSidebar}

\subparagraph{Preživetje}
GT te lahko prosi, da izvedeš preizkus Modrosti (Preživetje), ko slediš stopinjam, loviš divjad, vodiš svojo druščino skozi zimsko puščo, opaziš znake, da v bližini živijo sovji medvedi, napoveduješ vreme ali se poskušaš izogniti živemu pesku.

\subparagraph{Uvid}
Preizkus Modrosti (Uvid) določa, ali lahko sprevidiš resnične namere bitja, na primer ugotoviš, ali laže, predvidiš njegovo naslednjo potezo ... Pomagaš si z bitjevo telesno govorico, govornimi značilnostmi in spremembami v obnašanju.

\subparagraph{Zaznavanje}
Preizkus Modrosti (Zaznavanje) določa, ali opaziš, slišiš ali drugače zaznaš prisotnost česa. Meri tvoje zavedanje okolice in ostrino tvojih čutov. Na primer, lahko poskusiš prisluškovati pogovoru za zaprtimi vrati ali pod odprtim oknom ali pa poslušati lazenje pošasti po gozdu. Lahko se trudiš opaziti reči, ki so zakrite ali zlahka spregledljive, najsi bodo to orki v zasedi ob poti, silaki v sencah uličice ali soj sveče pod zaprtimi skrivnimi vrati.

\subparagraph{Zdravilstvo}
S preizkusom Modrosti (Zdravilstvo) lahko oskrbiš umirajočega tovariša ali prepoznaš bolezen.

\subparagraph{Ostali preizkusi Modrosti}
GT bo nemara hotel preizkus Modrosti, ko se polotiš podobnih opravil, kot so:

\begin{itemize}
    \item Dobiš nepojasnjen občutek, da moraš nekaj določenega ukreniti.
    \item Prepoznaš, ali je na videz mrtvo ali živo bitje v resnici nemrtvec.
\end{itemize}

\subsubsection{Uročna sposobnost}

Kleriki za svojo uročno sposobnost uporabljajo Modrost. Uročna sposobnost jim pomaga določiti TR-je urokov, ki jih čarajo.

\subsection{Karizma}

Karizma meri sposobnost učinkovitega sporazumevanja z drugimi. Vanjo sta všteta denimo samozavest in zgovornost, lik z močno Karizmo pa je lahko na primer tudi očarljiv ali avtoritativen.

\subsubsection{Preizkusi Karizme}

Do preizkusa Karizme lahko pride, ko poskušaš vplivati na druge ali jih zabavati, ko poskušaš narediti vtis ali se prepričljivo zlagati ali ko poskušaš obvladati kočljivo družbeno situacijo. V Karizmo so vključene veščine Napeljevanje, Nastopanje, Ustrahovanje in Zavajanje.

\subparagraph{Napeljevanje}
Kadar eno ali več oseb poskušaš prepričati v kaj taktno, vljudno ali dobronamerno, te lahko GT prosi za preizkus Karizme (Napeljevanje). Tipično uporabiš Napeljevanje, ko hočeš splesti prijateljstva, postaviti prijazno prošnjo, se ravnati po bontonu ali na splošno želiš drugim dobro. Primeri: komornika prepričuješ, da tvojo druščino spusti h kralju, se pogajaš za premirje med dvema sprtima plemenoma ali navdihneš gručo meščanov.

\subparagraph{Nastopanje}
Preizkus Karizme (Nastopanje) določa, kako dobro zabavaš občinstvo z glasbo, plesom, igro, pripovedovanjem ali drugo obliko zabave.

\subparagraph{Ustrahovanje}
Kadar nad kom poskušaš uveljaviti svojo voljo, bodisi skozi grožnje, sovražno ravnanje ali fizično nasilje, te lahko GT prosi za preizkus Karizme (Ustrahovanje). Primeri so iztiskanje informacij iz jetnika, odvračanje uličnih huliganov od spopada ali žuganje hehetavemu vezirju z razbito steklenico, češ naj se malce zamisli nad svojimi odločitvami.

\subparagraph{Zavajanje}
Preizkus Karizme (Zavajanje) določa, ali lahko prepričljivo zakriješ resnico, bodisi z govorom bodisi z dejanji. To zavajanje lahko vključuje vse od nejasnega izražanja do navadne laži. Zavajanje običajno uporabimo v situacijah, ko pregovarjamo stražarja, pretentavamo trgovca, goljufamo pri kockanju, delujemo pod krinko, skušamo koga pomiriti s slepimi obljubami ali hladnokrvno lažemo.

\subparagraph{Ostali preizkusi Karizme}
GT lahko zaprosi za preizkus Karizme, ko se polotiš podobnih opravil, kot so:

\begin{itemize}
    \item Poiščeš najprimernejšo osebo za pogovor o novicah, govoricah in čenčah.
    \item Pomešaš se med ljudi, da dobiš občutek za ključne teme pogovora.
\end{itemize}

\newpage

\subsubsection{Uročna sposobnost}

Trubadurji, paladini, čudežniki in coprniki za svojo uročno sposobnost uporabljajo Karizmo. Uročna sposobnost jim pomaga določiti TR-je urokov, ki jih čarajo.

\section{Rešilni meti}

Rešilni met predstavlja poskus ubežati uroku, pasti, bolezni ali podobni grožnji. Običajno se za rešilni met ne odločiš sam/a, temveč si vanj prisiljen/a, ker je tvoj lik ali pošast v nevarnosti.

Rešilni met opraviš tako, da vržeš k20 in prišteješ ustrezen popravek sposobnosti. Na primer, za rešilni met Gibčnosti uporabiš popravek Gibčnosti.

Na rešilni met lahko vplivajo razne okoliščine, zato zanje lahko veljajo razni prištevki ali odbitki in olajšave ali otežitve, kakor pač določi GT.

Vsak razred je izveden v vsaj dveh rešilnih metih. Čarovnik, na primer, je izveden v rešilnem metu Inteligence. Kot velja že za izvedenstvo v veščinah, izvedenstvo v rešilnem metu omogoča, da mu dodamo izvedenski prištevek, ko ga naredimo. Tudi nekatere pošasti so izvedene v nekaterih rešilnih metih.

Težavnostni razred za rešilni met postavi učinek, zaradi katerega rešilni met opravljamo. Na primer, TR za rešilni met proti uroku določata uročna sposobnost in izvedenski prištevek osebe, ki je urok izvedla.

Rezultate uspešnega ali spodletelega rešilnega meta prav tako opisuje učinek, ki je rešilni met sprožil. Običajno uspešen met pomeni, da bitje ne utrpi nobene škode ali manjšo škodo kot sicer.