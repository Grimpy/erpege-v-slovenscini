\chapter{Pustolovstvo}

Spust v starodavno grobnico strahot, tek skozi temne, tesne uličice mesta Globokovodje, prebijanje skozi gosto džunglo Otoka trepeta -- vse to se nam lahko zgodi v igri Temnic in zmajev. Tvoj lik bo morda raziskoval opuščene razvaline in neraziskane dežele, razkrival temačne skrivnosti in zlovešče načrte ter pobijal krvoločne pošasti. In če se bo vse dobro izteklo, bo tvoj lik preživel in užival v sadovih svoje bogate nagrade, preden se bo znova odpravil na naslednjo pustolovščino.

To poglavje se osredotoča na temelje pustolovskega življenja. Sem spada vse od pravil potovanja do zapletenih socialnih interakcij. Tu so opisana tudi pravila počivanja in dejavnosti, ki jih lahko tvoj lik počne v prostem času po koncu ene pustolovščine in pred pričetkom nove.

Najsi pustolovci odkrivajo zaprašeno temnico ali kompleksne razmere na kraljevem dvoru, igra sledi enakemu ritmu, kot smo ga omenili že na začetku te knjige:

\begin{enumerate}
    \item GT opiše okolico.
    \item Igralci opišejo, kaj želijo storiti.
    \item GT opiše rezultate njihovih dejanj.
\end{enumerate}

Navadno GT za oris pustolovščine uporabi zemljevid, na katerem beleži napredek likov, medtem ko raziskujejo mračne hodnike ali gozdnate planjave. Včasih se dogajanje v veliki meri opira na potek časa in na dejanja pustolovcev, zato GT za sledenje napredku uporabi časovni trak ali diagram.

\section{Čas}

V okoliščinah, ko je čas pomemben, GT določi dolžino trajanja posameznega opravila. GT lahko uporabi različna časovna merila glede na kontekst situacije. V temnici, denimo, premikanje pustolovcev merimo v \textbf{minutah}. Približno eno minuto se plazijo po dolgem hodniku, še eno minuto porabijo za preverjanje, ali na vratih na koncu hodnika ni kakšne pasti, potem pa dobrih deset minut preiskujejo sobo za vrati, da bi našli kaj zanimivega ali dragocenega.

V mestu ali v divjini je pogosto primerneje meriti čas v \textbf{urah}. Pustolovci, ki neučakano hitijo proti osamljenemu stolpu sredi gozda, premerijo tistih 24 kilometrov v slabih štirih urah.

Za daljša potovanja je čas najbolje šteti v \textbf{dnevih}. Za pot od Baldurjevih duri do Globokovodja pustolovci porabijo štiri dni, preden njihovo potovanje zmoti goblinska zaseda.

Med spopadom in v drugih bliskovitih situacijah se igra poslužuje \textbf{krogov}, ki trajajo po 6 sekund in so podrobneje razloženi v devetem poglavju.

\section{Gibanje}

V pustolovščinah T\&M-ja gibanje igra pomembno vlogo, naj bo to plavanje čez deročo reko, smukanje vzdolž hodnika temnice ali prečenje grozečega gorskega prelaza.

GT lahko povzame gibanje pustolovcev, ne da bi poračunal točne razdalje ali potovalne čase: \textquote{Potujete skozi gozd. Pozno zvečer tretjega dne najdete vhod v temnico.} Zlasti v temnici, še posebno v večji temnici ali v jamskem sistemu, lahko GT povzame gibanje med posameznimi naleti: \textquote{Potem ko ste ubili varuha vhoda v starodavno škratjo utrdbo, vas zemljevid vodi skozi več milj prostornih hodnikov do prepada, čez katerega vodi ozek, kamnit ločni most.}

Včasih je vendarle dobro vedeti, koliko traja pot od ene točke do druge, pa naj bodo to dnevi, ure ali minute. Potovalni čas določamo po dveh kriterijih: po hitrosti in potovalnem tempu gibajočih se bitij ter po terenu, po katerem se gibljejo.

\subsection{Hitrost}

Vsak lik ali pošast ima hitrost, ki predstavlja dolžino v metrih, ki jo lahko prehodi bitje v enem krogu. Tu gre predvsem za kratke trenutke silovitega gibanja, ko so od hitrosti odvisna življenja.

Spodnja pravila določajo, kako daleč se lahko lik ali pošast normalno premakne v minuti, v uri ali v dnevu.

\subsubsection{Potovalni tempo}

Med potovanjem se lahko skupina pustolovcev premika z normalnim, hitrim ali počasnim tempom, kot je prikazano v preglednici potovalnega tempa. V preglednici je zapisano tudi, kolikšno razdaljo lahko druščina prehodi v nekem časovnem obdobju in ali ima tempo kakšne stranske učinke. Hiter tempo zmanjša likovo pozornost na okolico, počasen tempo pa omogoča, da se liki tihotapijo naokrog in podrobneje preiščejo okolico (za več informacij glej razdelek \textquote{Dejavnosti med potovanjem} pozneje v tem poglavju).

\subparagraph{Hitra hoja}
Preglednica potovalnega tempa predpostavlja, da liki potujejo 8 ur na dan. Dnevni potovalni čas lahko tudi podaljšajo, vendar s tem tvegajo izčrpanost.

Vsako dodatno uro nad standardnimi 8 urami liki prehodijo razdaljo, ki je zapisana v stolpcu \textquote{Razdalja v eni uri} ob njihovem tempu. Na koncu vsake ure mora vsak lik izvesti rešilni met Konstitucije. TR zanj je 10 + 1 za vsako uro nad 8 ur. Če rešilni met spodleti, lik utrpi eno stopnjo izčrpanosti (glej dodatek A).

\subparagraph{Prevozna sredstva}
Za krajši čas (do ene ure) se številne živali premikajo mnogo hitreje od človečnjakov. Lik na konju lahko galopira približno eno uro in tako pride dvakrat dlje, kot bi prišel peš v hitrem tempu. Če liki na vsakih 8 do 10 milj dobijo novo spočito žival za jahanje, lahko pri istem tempu opravijo daljše razdalje, vendar so take okoliščine izjemno redke, razen v gosto poseljenih območjih.

Liki na vozovih, v kočijah ali drugih kopenskih vozilih si tempo izberejo. Liki v nadvodnih plovilih so omejeni na hitrost plovila (glej peto poglavje) in ne utrpijo slabosti hitrega tempa ali pridobijo prednosti počasnega tempa. Nekatere ladje lahko potujejo tudi do 24 ur na dan, vendar je to odvisno od plovila in velikosti posadke.

Nekatere jahalne živali, denimo pegaz ali grifon, in posebna vozila, kot je leteča preproga, ti omogočajo urnejše potovanje. O teh posebnih metodah prevoza piše več v \textit{Vodiču Gospodarja temnic}.

\begin{DndTable}[header=Potovalni tempo]{lcccX}
    \setcounter{rownum}{0}
    \multirow{2}{*}{\textbf{Tempo}} & \multicolumn{3}{c}{\textbf{Razdalja v enem/eni ...}} & \multirow{2}{*}{\textbf{Učinek}} \\
    & \textbf{minuti} & \textbf{uri} & \textbf{dnevu} & \\
    Hiter & 120 m & 6 km & 45 km & -5 odbitka k preizkusom pasivne Modrosti (Zaznavanje) \\
    Navaden & 90 m & 4,5 km & 36 km & \\
    Počasen & 60 m & 3 km & 27 km & Možnost uporabe prikritosti
\end{DndTable}

\subsubsection{Zahteven teren}

Hitrosti v preglednici potovalnega tempa predvidevajo, da se pustolovci gibljejo po relativno lahko prehodnem terenu: cestah, širnih planjavah ali prosto prehodnih hodnikih. Toda junaki se pogosto prebijajo skozi zarasle gozdove, globoke močvare, raztreščene ruševine, strme gore in poledenelo puščo. Vse to je zahteven teren.

Na zahtevnem terenu se premikaš s polovično hitrostjo -- za 1 prehojen meter po zahtevnem terenu sta potrebna 2 metra hitrosti --, zato lahko prehodiš le polovico normalne dolžine v minuti, uri ali dnevu.

\subsection{Posebni tipi gibanja}

Pomikanje skozi nevarne temnice ali divje dežele zajema več kot le hojo. Pustolovci morajo na svoji poti često tudi plezati, se plaziti, plavati ali skakati.

\subsubsection{Plezanje, plavanje in plazenje}

Vsak prehojen meter stane 1 dodaten meter (2 dodatna metra na zahtevnem terenu), ko plezaš, plavaš ali se plaziš. Če imaš definirano hitrost plezanja, s katero plezaš, ali hitrost plavanja, s katero plavaš, ti te dodatne cene ni treba upoštevati. Če se GT tako odloči, plezanje po zdrsljivi navpični površini ali površini z redkimi oprijemi zahteva uspešen preizkus Moči (Atletika). Podobno plavanje po razburkani vodi morda zahteva uspešen preizkus Moči (Atletika).

\subsubsection{Skakanje}

Dolžino oziroma višino skoka določa tvoja Moč.

\subparagraph{Skok v daljino}
Ko skočiš v daljino, lahko preskočiš število metrov, enako tvoji oceni Moči krat 0,3 (oziroma 1 meter na 3 točke Moči), če si vzel/a vsaj 3-metrski zalet. Če skočiš z mesta, lahko preskočiš le pol te razdalje. V vsakem primeru vsak preskočen meter stane en meter premika.

To pravilo predpostavlja, da višina tvojega skoka ni pomembna, kot velja recimo pri skoku čez potok ali prepad. Če GT dovoli, lahko poskusiš preskočiti nizko oviro (nič višjo od četrtine dolžine skoka), na primer živo mejo ali nizek zid, za kar ti mora uspeti preizkus Moči (Atletika) s TR 10.

Če po skoku pristaneš na zahtevnem terenu, se obdržiš na nogah le, če ti uspe preizkus Gibčnosti (Akrobatika) s TR 10. Sicer padeš po tleh.

\subparagraph{Skok v višino}
Ko skočiš v višino, šineš toliko metrov visoko, koliko je (3 + tvoj popravek Moči) krat 0,3 (najmanj 0 metrov), če si prej vzel/a vsaj 3-metrski zalet. Če skočiš z mesta, lahko skočiš le pol tako visoko. V vsakem primeru sleherni meter preskočene višine stane en meter premika. V nekaterih okoliščinah ti lahko GT dovoli, da opraviš preizkus Moči (Atletika) in skočiš višje kot običajno.

Med skokom lahko iztegneš roke nadse za polovico svoje višine. Tako lahko z rokami dosežeš višino, ki je enaka višini skoka plus 1,5 krat tvoja višina.

\subsection{Dejavnosti med potovanjem}

Dokler pustolovci potujejo skozi temnico ali divjino, morajo biti na preži za nevarnostmi, nekateri pa počnejo vmes tudi druge stvari, ki pripomorejo k uspešnemu popotovanju.

\begin{DndSidebar}{Delitev druščine}
Včasih je smiselno druščino razdeliti na dve ali več skupin, še zlasti, če hočete, da en ali dva lika gresta v izvidnico. Skupine se lahko premikajo z različnimi hitrostmi. Vsaka skupina ima svoje čelo, sredino in rep.
    
Slabost tega pristopa je, da bo druščina v primeru napada ločena v več manjših skupinic. Prednost je, da se bo majhna skupinica prikritih likov, ki se pomikajo počasi, morda lahko pretihotapila mimo sovražnikov, ki bi jih nerodnejši liki opozorili nase. Par počasnih rokomavhov je veliko težje zaznati, če njun škratji bojevniški tovariš ostane zadaj.
\end{DndSidebar}

\subsubsection{Kolona}

Pustolovci naj med potovanjem naredijo kolono. Tako je mogoče kasneje lažje določiti, kdo vse se ujame v pasti, kdo lahko opazi skrite sovražnike in kdo je najbližje pošastim, ko se vname boj.

Lik lahko hodi na čelu kolone (prva vrsta), nekje v sredini (sredinske vrste) ali na repu kolone (zadnja vrsta). Na čelu in na repu lahko hodi več likov vštric, pri čemer za to potrebujejo dovolj prostora. Če je steza preozka, se mora kolona ustrezno preoblikovati. V tem primeru se običajno nekateri liki prestavijo v sredino kolone.

\subparagraph{Manj kot tri vrste}
Če druščina hodi v koloni, sestavljeni le iz dveh vrst, sta to rep in čelo kolone. Če vsi hodijo vštric v eni vrsti, tej rečemo čelo kolone.

\subsubsection{Prikritost}

Kadar potujejo v počasnem tempu, se lahko liki pomikajo prikrito. Dokler niso na odprtem, lahko poskusijo presenetiti bitja, na katera naletijo, ali se jim izogniti. Preberi še pravila za skrivanje v sedmem poglavju.

\subsubsection{Zaznavanje groženj}

Za preverjanje, ali kdo v skupini zazna skrito grožnjo, uporabite ocene pasivne Modrosti (Zaznavanje) likov. GT se lahko odloči, da grožnjo pogojno zaznajo le liki v določeni vrsti. Na primer, ko liki raziskujejo blodnjak predorov, se lahko GT odloči, da imajo le liki na repu kolone priložnost, da slišijo ali opazijo prikrito bitje, ki zasleduje skupino.

Dokler liki potujejo s hitrim tempom, njihove ocene pasivne Modrosti (Zaznavanje) za zaznavanje groženj utrpijo odbitek -5.

\subparagraph{Nalet na druga bitja}
Če GT določi, da pustolovci med potovanjem naletijo na druga bitja, je od obeh skupin odvisno, kaj sledi. Katerakoli od skupin se lahko odloči za napad, pogovor, beg ali čakanje na dejanja nasprotne skupine.

\subparagraph{Presenečanje nasprotnikov}
Če pustolovci naletijo na sovražno bitje ali skupino, GT določi, ali pustolovce oziroma njihove nasprotnike spopad preseneti. Več o tem v devetem poglavju.

\subsubsection{Druge dejavnosti}

Liki, ki se med potjo posvečajo drugim opravilom, niso pozorni na morebitne nevarnosti. Ti liki ne prispevajo svojih ocen Modrosti (Zaznavanje) k skupni verjetnosti, da bo skupina opazila skrite grožnje. Lik, ki ne opreza za nevarnostmi, lahko počne nekaj od spodaj naštetega ali, z GT-jevim dovoljenjem, tudi kaj drugega.

\subparagraph{Stezosledstvo}
Lik lahko poskusi preprečiti, da bi druščina zašla, in opravi preizkus Modrosti (Preživetje), ko GT tako zahteva. (V \textit{Vodiču Gospodarja temnic} piše, kako določiti, kdaj se druščina izgubi).

\subparagraph{Kartografija}
Lik lahko nariše zemljevid, ki beleži prehojeno pot in pustolovcem pomaga najti pravo pot, če so zašli. Za to ni treba opraviti nobenega preizkusa sposobnosti.

\subparagraph{Zasledovanje}
Lik lahko sledi stopinjam drugega bitja. Za to opravi preizkus Modrosti (Preživetje), ko GT tako zahteva. (V \textit{Vodiču Gospodarja temnic} so navedena pravila zasledovanja).

\subparagraph{Oskrba}
Lik lahko opreza za morebitnimi viri hrane in vode. Za to opravi preizkus Modrosti (Preživetje), ko GT tako zahteva. (V \textit{Vodiču Gospodarja temnic} so navedena pravila oskrbovanja).

\section{Okolje}

Pustolovstvo vključuje spuste v temačne in nevarne kraje, polne vabljivih skrivnosti. Pravila v tem razdelku pokrivajo nekaj najpomembnejših vrst stikov pustolovcev z okolico. V \textit{Vodiču Gospodarja temnic} so navedena pravila za razmere, ki so manj običajne.

\subsection{Padanje}

Največkrat pustolovca ogrozi padec z velike višine.

Ko bitje pristane, utrpi 1k6 škode zaradi topega udarca za vsake 3 metre padca. Utrpi lahko največ 20k6 škode. Bitje je po pristanku v ležečem stanju (na tleh), razen če se nekako izogne poškodbam zaradi padca.

\subsection{Dušenje}

Bitje lahko zadržuje dih za število minut, enako 1 + popravek Konstitucije (najmanj 30 sekund).

Če bitju zmanjka sape ali če se davi, lahko preživi število krogov, enako popravku Konstitucije (najmanj 1 krog). Na začetku naslednjega kroga izgubi vse življenjske točke in začne umirati. Od tedaj si ne more več povrniti življenjskih točk in ga ni mogoče stabilizirati, dokler znova ne zadiha.

Na primer, bitje z oceno Konstitucije 14 lahko drži sapo 3 minute. Če se začne dušiti, ima 2 kroga, da zadiha, preden mu število življenjskih točk pade na 0.

\subsection{Vid in svetloba}

Nekaj najbolj bistvenih pustolovskih opravil -- recimo zaznavanje nevarnosti, iskanje skritih predmetov, napadanje sovražnikov ali usmerjanje urokov -- se v veliki meri zanaša na likov vid. Tema in drugi učinki, ki omejujejo vid, so lahko za pustolovca pravi trn v peti.

Območje je lahko rahlo ali pošteno nepregledno. V \textbf{rahlo nepreglednem} območju, za katerega so krivi na primer medla osvetljenost, rahla megla ali srednje gosto ščavje, imajo bitja otežitev na vse preizkuse Modrosti (Zaznavanje), ki se zanašajo na vid.

\textbf{Pošteno nepregledno} območje, torej območje teme, goste megle ali zelo gostega rastja, povsem zablokira pogled. Bitje se takrat znajde v stanju zaslepljenosti (glej dodatek A).

Prisotnost ali odsotnost svetlobe v okolju povzroča tri vrste osvetljenosti: močno svetlobo, medlo svetlobo in temo.

\textbf{Močna svetloba} omogoča, da večina bitij vidi normalno. Celo bolj mračni dnevi nudijo močno svetlobo. Drugi viri močne svetlobe so še bakle, lanterne, ogenj in druga svetila znotraj določenega obsega.

\textbf{Medla svetloba}, tudi zasenčenost, ustvari rahlo nepregledno območje. Območje medle svetlobe je običajno ločnica med izvorom močne svetlobe, na primer baklo, in okoliško temo. Pod medlo svetlobo štejemo tudi medel soj somraka in svita. Tudi posebno bleščeča polna luna lahko deželo obsije z medlo svetlobo.

\textbf{Tema} ustvari pošteno nepregledno območje. Prisotna je ponoči zunaj (tudi v jasnih nočeh), v zaprtih neosvetljenih prostorih, v podzemlju ali v območju čarobne teme.

\subsubsection{Slepovid}

Slepogledo bitje lahko svojo bližnjo okolico v določenem dometu vidi tudi brez zanašanja na oči. Slepovid imajo bitja brez oči, na primer žlobudre, in bitja z eholokacijo ali izostrenimi čuti, kot so netopirji in pravi zmaji.

\subsubsection{Temovid}

Mnoga bitja, ki bivajo v svetovih TiZ-a, še zlasti podzemna, imajo temovid. Do določene razdalje lahko temogledo bitje vidi v medli svetlobi, kot da bi bila močna svetloba, in v temi, kot da bi bil medla svetloba. To pomeni, da so določena območja v temi, kar se tega bitja tiče, zgolj rahlo nepregledna. Toda celo tedaj bitje ne more razločiti posameznih barv, le odtenke sivine.

\subsubsection{Vsevid}

Vsegledo bitje lahko do določene razdalje vidi v navadni in čarobni temi, vidi nevidna bitja in predmete, privzeto spozna privide in proti njim uspešno izvede rešilne mete ter spozna prvotno obliko preobraženca ali bitja, ki ima z magijo spremenjen videz. Poleg tega ima vsegledo bitje vpogled v Etrno planoto.

\subsection{Hrana in voda}

Liki, ki ne jejo ali pijejo, trpijo za izčrpanostjo (glej dodatek A). Izčrpanosti, ki jo povzročita hrana ali voda, ni mogoče odpraviti, dokler lik ne poje in popije potrebne količine živeža.

\subsubsection{Hrana}

Lik potrebuje približno pol kilograma hrane na dan, pri čemer lahko svojo hrano razdeli na polovične dnevne obroke in tako podaljša rok porabe hrane. Če poje le četrt kilograma hrane na dan, to šteje kot polovica dneva brez hrane.

Lik lahko preživi brez hrane za število dni, enako 3 + popravek Konstitucije (najmanj 1). Ko prekorači to mejo, na koncu vsakega naslednjega dne utrpi eno stopnjo izčrpanosti.

Navaden dan, ko lik jé, ponastavi število dni brez hrane nazaj na nič.

\subsubsection{Voda}

Lik potrebuje 4,5 litra (eno galono) vode na dan ali 9 litrov (dve galoni) vode na vroč dan. Lik, ki popije le pol toliko vode, mora opraviti rešilni met Konstitucije s TR 15, v nasprotnem primeru utrpi eno stopnjo izčrpanosti ob koncu dneva. Lik, ki spije še manj kot polovico potrebne količine vode, privzeto utrpi eno stopnjo izčrpanosti ob koncu dneva.

Če ima lik že eno ali več stopenj izčrpanosti, v vsakem primeru prejme dve stopnji.

\subsection{Interakcija s predmeti}

Likova interakcija s predmeti v okolici je pogosto enostavno rešljiva. Igralec GT-ju pove, kaj počne njegov lik, na primer da premakne vzvod, nato pa GT opiše, kaj -- če sploh kaj -- se zgodi.

Na primer, lik se lahko odloči pritisniti na vzvod, kar nemara povzroči, da se dvignejo dvižne duri, da se soba začne polniti z vodo ali da se odpro skrita vrata na bližnjem zidu. Če je vzvod zarjavel in zataknjen, bo moral lik uporabiti silo. V takih primerih lahko GT zahteva preizkus Moči, da določi, ali liku uspe premakniti vzvod. GT sam določi TR za tak preizkus glede na težavnost opravila.

Liki lahko s svojimi orožji in uroki predmete tudi poškodujejo. Predmeti so imuni na strup in poškodbe uma, drugače pa fizični in čarobni napadi nanje vplivajo prav tako kakor na bitja. GT odredi zaščitni razred in življenjske točke predmeta. Lahko se odloči tudi, da imajo nekateri predmeti odpornost ali imunost na določene vrste napadov. (Vrv je denimo zelo težko presekati s kijem.) Predmeti niso nikoli uspešni pri rešilnih metih Moči ali Gibčnosti in so imuni na učinke, ki zahtevajo druge rešilne mete. Ko predmet izgubi vse življenjske točke, se zlomi ali razbije.

Lik lahko poskusi predmet razbiti s preizkusom Moči. GT nastavi TR za ta preizkus.

\section{Medsebojni odnosi}

Za dobro pustolovščino so ključni raziskovanje temnic, premoščanje ovir in pobijanje pošasti. A prav tako pomembni so stiki, ki jih vzpostavljajo pustolovci z drugimi prebivalci sveta.

Interakcije so raznovrstne. Morda moraš brezvestnega tatu prepričati, naj prizna svojo krivdo, ali pihati na dušo jezni zmajevki, da ti bo prizanesla. Vlogo igralčevega sogovorca v takih primerih prevzame GT. Liki, ki jih igra GT in ne igralci, so \textbf{neigrane stranske osebe}.

Na splošno se stranske osebe do tebe obnašajo prijazno, ravnodušno ali sovražno. Prijazne stranske osebe ti veliko raje pomagajo, sovražne pa ti mešajo štrene. Kajpada je veliko lažje doseči svoje pri prijazni stranski osebi.

Družbene interakcije tvorita dve prvinski sestavini: igranje vlog in preizkušanje sposobnosti.

\subsection{Igranje vloge}

Kot že ime pove, gre pri igranju vloge za nastopanje. Ti kot igralec/ka poskušaš razmišljati, ravnati in govoriti kot tvoj lik.

Igranje vlog je v igri TiZ-a vseprisotno, še najbolj izrazito pa je prav med sporazumevanjem z drugimi bitji. Kaprice, obnašanje in osebnost tvojega lika vplivajo na izid interakcij.

Kot tvoj lik lahko nastopaš na dva načina: na opisni in dejanski. Večina igralcev uporablja mešanico obeh. Izbiraš in prepletaš ju lahko tako, kot ti najbolj ustreza.

\subsubsection{Opisno igranje}

Ko uporabiš ta pristop, soigralcem in GT-ju opišeš govor in dejanja svojega lika. V mislih si ustvariš podobo svojega lika in drugim predstaviš, kaj počne.

Podajmo primer. Klemen igra škrata Tordeka. Tordek je hitre jeze in za tegobe svoje družine krivi vilince iz Gostolesa. V gostilni poskuša zoprn vilinski muzikant, ki prisede za Tordekovo mizo, z njim načeti pogovor.

Klemen reče: \textquote{Tordek pljune po tleh, zakolne proti trubadurju in odtopota k točilnemu pultu. Sede na stol in se srepo zastrmi proti muzikantu, preden naroči še eno pijačo.}

V tem primeru je Klemen dobro nakazal Tordekovo razpoloženje. Sedaj ima GT jasno predstavo o škratovem obnašanju in dejanjih.

Ko uporabljaš opisni pristop, imej v mislih, da:

\begin{itemize}
    \item moraš opisati čustva in stališča svojega lika,
    \item se moraš osredotočiti na ravnanje svojega lika in na to, kako ga lahko dojemajo drugi,
    \item lahko povedano olepšaš in opišeš tako slikovito, kot se ti zdi primerno.
\end{itemize}

Ne obremenjuj se s točnostjo opisov in obnašanja lika. Osredotoči se le na to, kaj bi tvoj lik v danih okoliščinah storil, in opiši, kar si predstavljaš.

\subsubsection{Dejansko igranje}

Medtem ko z opisnim pristopom svojim soigralcem poveš, kaj tvoj lik misli in počne, z dejanskim igranjem to pokažeš.

Ko dejansko nastopaš, govoriš z glasom svojega lika kot gledališki ali filmski igralec, ki \textquote{igra} tvoj lik. Lahko celo oponašaš gibanje svojega lika. Ta pristop prispeva več k vživetosti v dogajanje kot opisno igranje, vendar je še vedno treba opisati stvari, ki jih ne moremo zaigrati.

Če se vrnemo na primer Klemnovega igranja Tordeka, si oglejmo, kako bi bil videti zgornji prizor, če bi Klemen uporabil dejansko igranje.

Klemen si nadene Tordekov globok, osoren glas in bevskne: \textquote{Mi je bilo že čudno, kaj tukaj tako grozno smrdi. Če bi te hotel poslušati, bi ti polomil roko in se naslajal ob tvojem vreščanju.} Nato v svojem običajnem glasu Klemen nadaljuje: \textquote{Vstanem, vilinca prebodem s srepim pogledom in se odpravim proti točilnemu pultu.}

\subsubsection{Rezultati igranja vloge}

GT na podlagi dejanj in atributov tvojega lika določi, kako se stranska oseba odzove. Bojazljiva oseba bo klonila pod grožnjami z nasiljem. Trmasta škratica se ne bo pustila nadlegovati. Naduta zmajevka se bo ob laskanju omehčala.

Ko se sporazumevaš s stransko osebo, bodi pozoren/a na to, kako GT predstavlja osebino razpoloženje, govor in osebnost. Če ti uspe ugotoviti, kakšna je oseba po naravi, kakšne ideale, hibe in vezi ima, lahko z njihovo pomočjo vplivaš na njeno obnašanje.

Interakcije v TiZ-u so blazno podobne interakcijam v resničnem življenju. Če uporabiš prave besede, postaviš stranskim osebam mamljivo ponudbo, jim zagroziš z nečim, česar se bojijo, ali obrneš njihova čustva in cilje v svoj prid, lahko dosežeš praktično karkoli. Po drugi strani pa tvoja prizadevanja ne bodo obrodila sadov, če užališ oholega bojevnika ali osmešiš plemičeve zaveznike.

\subsection{Preizkusi sposobnosti}

Poleg igranja vlog so preizkusi sposobnosti ključni za ugotavljanje izida interakcije.

Tvoji poskusi igranja vloge lahko spremenijo obnašanje stranske osebe, vendar je uspeh še vedno lahko odvisen od verjetnosti. Na primer, GT lahko kadarkoli med sporazumevanjem zahteva preizkus Karizme, če hoče odziv stranske osebe določiti s pomočjo metanja kock. V nekaterih primerih lahko izvedeš tudi preizkus kake druge sposobnosti, če GT tako odloči.

Ko se sporazumevaš s stransko osebo, bodi pozoren/a na svoje izvedenstvo v veščinah in izberi pristop, ki izrablja tvoje najvišje prištevke in ocene veščin. Če mora druščina prelisičiti stražarja, da jih spusti v grad, bo rokomavh, ki je izveden v Zavajanju, najbolj primeren za to, da vodi pogovor. Kadar se pogajate za izpust talcev, bo najbolje, da komunikacijo prek Napeljevanja opravi klerik.

\section{Počivanje}

Še tako junaški pustolovci ne morejo ves dan samo raziskovati, se družiti z bitji in se bojevati. Potrebujejo počitek -- čas za spanje in prehranjevanje, okrevanje, zbistritev misli in duha -- za čaranje ter za priprave na nadaljnje pustolovščine.

Pustolovci, kot tudi druga bitja, lahko opravljajo krajše počitke tekom dneva in daljši počitek ob koncu dneva.

\subsection{Krajši počitek}

Krajši počitek je vsaj enourno obdobje, v katerem liki ne počnejo nič zahtevnejšega od prehranjevanja, pitja, branja in okrevanja.

Lik lahko ob koncu krajšega počitka porabi eno ali več življenjskih kock (največ toliko, kolikor jih ima -- se pravi toliko, kolikršna je stopnja lika). Za vsako porabljeno kocko igralec vrže to kocko in ji prišteje popravek Konstitucije. Lik si povrne toliko življenjskih točk (najmanj 0). Igralec se lahko odloči porabiti še eno življenjsko kocko po vsakem metu. Lik si povrne del porabljenih življenjskih kock po daljšem počitku, kot je razloženo spodaj.

\subsection{Daljši počitek}

Daljši počitek je vsaj osemurno obdobje, v katerem liki najmanj 6 ur spijo in se največ 2 uri ukvarjajo z nezahtevnimi dejavnostmi, denimo branjem, klepetanjem, obedovanjem ali straženjem. Če počitek prekine zahtevnejša dejavnost -- vsaj 1 ura hoje, borbe, čaranja ali podobnega pustolovskega udejstvovanja --, morajo liki ponoviti počitek od začetka, če hočejo kaj koristi od njega.

Po koncu daljšega počitka si lik povrne vse izgubljene življenjske točke. Lik si povrne tudi nekaj življenjskih kock. Nazaj dobi največ toliko življenjskih kock, kolikor je polovica skupnih likovih življenjskih kock (najmanj 1 kocka). Primer: če ima lik osem življenjskih kock, si lahko po daljšem počitku povrne največ štiri življenjske kocke.

V razponu 24 ur lik nima koristi od več kot enega daljšega počitka. Da mu počitek podeli vse zgornje ugodnosti, mora imeti lik vsaj 1 življenjsko točko ob začetku počitka.

\section{Med pustolovščinami}

Kadar se ne podajajo v temnice in v vojne proti starodavnemu zlu, morajo pustolovci najti čas za počitek, okrevanje in priprave na naslednjo pustolovščino. Mnogi pustolovci izrabijo ta čas tudi za druga opravila, na primer za izdelovanje orožij in ščitov, učenje in eksperimentiranje ali zapravljanje svojega težko prisluženega zlata.

V nekaterih primerih se pretečenemu času ne posveti posebne pozornosti. Ko se prične nova pustolovščina, GT enostavno naznani, da je preteklo nekaj časa, in pusti igralcem, da opišejo, kaj so njihovi liki medtem počeli. Včasih pa GT zahteva točne časovne intervale, da lahko pravilno uskladi skrite, pustolovcem nevidne dogodke, ki so pomembni za zgodbo.

\subsection{Cena življenjskih slogov}

V času, ko nisi na pustolovščini, si izbereš, kako boš živel/a, in plačaš ceno za ta življenjski slog, kot je opisano v petem poglavju.

Življenjski slog ne vpliva znatno na tvoj lik, je pa lahko pomemben za tvojo družbeno podobo. Na primer, če živiš gosposko, ti bodo mestni plemiči verjetno resneje prisluhnili, kot če živiš v revščini.

\subsection{Prostočasne dejavnosti}

Kadar nisi na pustolovščini, te lahko GT vpraša, kaj tvoj lik počne v svojem prostem času. Obdobja prostega časa so lahko različno dolga, vendar vsaka prostočasna dejavnost za izpolnitev zahteva določeno število dni, pri čemer štejejo samo dnevi, ko posamezno dejavnost opravljaš vsaj 8 ur. Ni nujno, da so dnevi zaporedni. Če ti poleg nujnega števila dni za to dejavnost ostane več neporabljenih dni, se lahko preostale dni še naprej ukvarjaš z isto dejavnostjo ali pričneš z drugo.

Poleg spodnjih je možnih še več drugih prostočasnih dejavnosti. Če želiš, da tvoj lik v prostem času počne kaj, kar tukaj ni zapisano, se o tem posvetuj s svojim GT-jem.

\subsubsection{Rokodelstvo}

Lahko izdeluješ nemagične predmete, tudi pustolovsko opremo in umetnine. Izveden/a moraš biti z orodjem v povezavi s predmetom, ki ga izdeluješ (to je po navadi obrtniško orodje). Imeti moraš dostop do posebnega materiala ali prostorov, ki so potrebni za izdelavo predmeta. Na primer, nekdo, ki je izveden s kovaškim orodjem, potrebuje kovačnico, da v njej izdela meč ali oklep.

Vsak dan, ki ga preživiš ob rokodelstvu, lahko izdelaš enega ali več predmetov, ki skupaj ne presežejo vrednosti 5 zlatnikov, pri čemer porabiš toliko surovih materialov, da je njihova skupna cena enaka polovici skupne vrednosti izdelkov. Če želiš izdelati nekaj, kar je na trgu vredno več kot 5 zlatnikov, vsak dan izdelaš del izdelka v vrednosti 5 zlatnikov in to počneš toliko časa, dokler ne dosežeš tržne vrednosti izdelka. Na primer, ploščeni oklep (tržna cena je 1.500 zl) boš sam/a izdeloval/a 300 dni.

Več likov lahko združi moči in izdela en sam predmet, če so vsi liki izvedeni z ustreznim orodjem in delajo v istem prostoru. Vsak lik doprinese del izdelka v vrednosti 5 zlatnikov na dan. Na primer, trije liki, ki so izvedeni z ustreznim orodjem in delajo v ustrezni delavnici, lahko izdelajo ploščeni oklep v 100 dneh, za kar porabijo 750 zlatnikov.

Med rokodelsko dejavnostjo lahko živiš skromno, ne da bi ti bilo treba plačati 1 zlatnik na dan, ali udobno življenje za polovico normalne cene (več o življenjskih slogih in njihovi potrošnji piše v petem poglavju).

\subsubsection{Zaposlitev}

Kadar nisi na pustolovščini, se lahko zaposliš in tako živiš skromno, ne da bi ti bilo treba plačati 1 zlatnik na dan (več o življenjskih slogih in njihovi potrošnji piše v petem poglavju). Ta ugodnost velja, dokler si zaposlen/a.

Če si član/ica organizacije, ki ponuja plačano službo, kot so na primer svetišča ali cehi, zaslužiš dovolj za udobno življenje.

Če si izveden/a v Nastopanju in to veščino uporabiš v prostem času, lahko zaslužiš dovolj za bogato življenje.

\subsubsection{Okrevanje}

Prosti čas lahko izrabiš za to, da si opomoreš od hujše poškodbe, bolezni ali zastrupitve.
Po treh dneh okrevanja lahko izvedeš rešilni met Konstitucije s TR 15. Če je met uspešen, izbereš enega izmed spodnjih izidov:

\begin{itemize}
    \item Izničiš en učinek, ki ti preprečuje, da si povrneš življenjske točke.
    \item Naslednjih 24 ur imaš olajšavo na rešilne mete proti eni bolezni ali strupu, ki te trenutno pesti.
\end{itemize}

\newpage

\subsubsection{Proučevanje}

Prosti čas predstavlja odlično priložnost za preiskovanje skrivnosti, ki jih je druščina našla tekom kampanje. Proučevanje lahko vključuje preletavanje zaprašenih knjig in krhkih zvitkov v knjižnici ali plačevanje pijače krajanom, da bi iz njih izvlekel/a lokalne govorice in čenče.

Ko pričneš z raziskavami, GT določi, ali so ti informacije na voljo, koliko dni jih boš iskal/a in ali ti  kaj omejuje proučevanje (na primer da je potrebno poiskati določenega posameznika, knjigo ali kraj). GT lahko zahteva tudi, da izvedeš enega ali več preizkusov sposobnosti, denimo preizkus Inteligence (Preiskovanje), da najdeš sledi, ki te bodo pripeljale do pravih informacij, ali preizkus Karizme (Napeljevanje), da ti nekdo pomaga pri raziskavah. Ko je tem pogojem zadoščeno, prejmeš ustrezno znanje, če je na voljo.

Za vsak dan raziskav moraš porabiti 1 zlatnik. Ta cena ni vključena v tvoje normalne stroške življenjskega sloga (opisano v petem poglavju) in jo moraš plačati ločeno.

\subsubsection{Urjenje}

Prosti čas lahko porabiš za učenje novega jezika ali urjenje s kakim orodjem. Tvoj GT lahko ponudi še druge vadbene možnosti.

Najprej poiščeš učitelja, ki te bo pripravljen učiti. GT določi, koliko časa urjenje traja in ali je potrebno zanj izvesti kakšen preizkus sposobnosti.

Urjenje traja 250 dni in terja 1 zlatnik na dan. Po končanem urjenju si se uspešno naučil/a nečesa novega.