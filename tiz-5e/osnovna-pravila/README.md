# Ljubiteljski prevod 5. izdaje TiZ

Ljubiteljski prevod po uradnih [Osnovnih pravilih](https://dnd.wizards.com/what-is-dnd/basic-rules) in dodatnem materialu s spletišča [DND Beyond](https://www.dndbeyond.com/sources/basic-rules).

## Zasluge

* Slika ozadja iz [Lost and Taken](https://lostandtaken.com/)
* LaTex šablona: https://github.com/rpgtex/DND-5e-LaTeX-Template
* Nekaj pomembnejših oseb/skupin, ki so pripomogle k nastanku prevoda:
    - [Domišljijski slovarček Tamiusa Hana](http://domišljijski-slovarček.tamius.net/brskaj),
    - [Krčma - Discord skupnost, kjer je že veliko dobrih ljudi dalo krasne nasvete za prevajanje](https://discord.gg/44CBBtshDd),
    - [Domišljijski slovarček Krofa Drakule](https://krofdrakula.github.io/dnd-translations/?lang=sl),
    - in vsi drugi, ki prispevate ideje, predloge in spodbudo :)