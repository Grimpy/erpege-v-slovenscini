# Več ljubiteljskih prevodov za igro TiZ

Ves originalni material je prosto dostopen na spletu iz uradnih virov.

## Struktura
* V mapi `grafike` so slike in izrezki za rabo v različnih dokumentih. Ločeni so, da jih je lažje deliti med več dokumenti. Tako tudi šparam s prostorom.
* V mapi `osnovna-pravila` je prevod Osnovnih pravil, spisan v LaTexu. PDF je na voljo [tukaj](/tiz-5e/osnovna-pravila/basic_rules.pdf).
* V mapi `pustolovski-obrazec` je preveden t.i. "character sheet". PDF je na voljo [tukaj](/tiz-5e/pustolovski-obrazec/pustolovski_obrazec.pdf).