# Ljubiteljski prevod kratkih navodil za 7. izdajo Cthulhujevega klica

Tu je prevod kratkih navodil za igro Cthulhujev klic, ki so na voljo [na Chaosiumovem spletišču](https://www.chaosium.com/content/FreePDFs/CoC/CHA23131%20Call%20of%20Cthulhu%207th%20Edition%20Quick-Start%20Rules.pdf).