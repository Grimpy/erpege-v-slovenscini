# Ljubiteljski prevod 7. izdaje Cthulhujevega klica

* V mapi `kratka-navodila` se nahaja prevod kratkih pravil za začetek igranja. PDF navodil je na voljo [tukaj](/cthulhujev-klic-7e/kratka-navodila/kratka-navodila.pdf).
* V mapi `preiskovalska-listina` je preveden t.i. "investigator sheet". PDF je na voljo [tukaj](/cthulhujev-klic-7e/preiskovalska-listina/Cthulhujev_klic_Preiskovalska_listina.pdf).